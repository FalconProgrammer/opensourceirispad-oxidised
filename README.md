# Open Source Iris PAD Oxidised

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6499164.svg)](https://doi.org/10.5281/zenodo.6499164)

This is a Rust implementation based on both [OpenSourceIrisPAD](https://github.com/CVRL/OpenSourceIrisPAD) which is based on Doyle [Robust Detection of Textured Contact Lenses in Iris Recognition Using BSIF](https://ieeexplore.ieee.org/document/7264974/). This repository extends the original implementation by also having an additional phase which determines the best model combinations overall.

This repository also provides an API for use with other Rust applications that is capable of loading up a majority vote model.

This project was initially developed under the EU Horizon 2020 project [D4FLY](https://d4fly.eu/).

# Requirements

- OpenCV will need to be installed in the system and available. Look at [OpenCV rust instructions](https://github.com/twistedfall/opencv-rust#getting-opencv) on how to install it correctly. 
- A Rust compiler: https://www.rust-lang.org/tools/install

# Compilation

To build the primary executable, run the following command.

``` sh
cargo build --release
```

This will produce the executable in the target/release folder.


# Usage

The compiled executable requires a valid config file sitting in the same working directory called *config.toml*. A default config can be found [here](default_config/config.toml) - this will need to be modified to suit your system.

The training set/testing set files are CSV files excluding a header with two fields for each row: *filename, label*. Filename must be a full/relative path to the image, with label being a 0 or 1.

Once you have a working config file, you can run the executable produced from compilation!
