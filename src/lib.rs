#![allow(dead_code)]

#[macro_use]
extern crate lazy_static;

pub mod bsif_filters;
pub mod feature_extractor;

pub mod config;
pub mod model_details;
mod pad_auto_trainers;
pub mod pad_detector;

pub mod csv_helpers {
	pub fn load_csv_file_list(file: &str) -> anyhow::Result<Vec<(String, i32)>> {
		let mut reader = csv::Reader::from_path(file)?;

		Ok(reader
			.records()
			.filter_map(|line| match line {
				Ok(l) => {
					if l.len() != 2 {
						None
					} else {
						Some((l[0].to_string(), l[1].parse::<i32>().ok()?))
					}
				}
				Err(_) => None,
			})
			.collect())
	}
}
