use itertools::Itertools;
use std::collections::HashSet;

pub mod filters;
pub mod filters_specific;

macro_rules! filter_map{
    ($filtersize:tt, $bitsize:tt) => {
        concat_idents::concat_idents!(fname = FILTER_,$filtersize,_,$filtersize,_,$bitsize {
            $crate::bsif_filters::const_filters::filters::fname
        })
    }
}

macro_rules! filter_specific_map{
    ($filtersize:literal, $bitsize:literal) => {
        concat_idents::concat_idents!(fname = FILTER_,$filtersize,_,$filtersize,_,$bitsize {
            $crate::bsif_filters::const_filters::filters_specific::fname
        })
    }
}

lazy_static! {
	pub static ref FILTER_MAP: std::collections::HashMap<(u8, u8), &'static [f64]> = {
		let mut m = std::collections::HashMap::new();
		m.insert((11, 10), filter_map!(11, 10));
		m.insert((11, 11), filter_map!(11, 11));
		m.insert((11, 12), filter_map!(11, 12));
		m.insert((11, 5), filter_map!(11, 5));
		m.insert((11, 6), filter_map!(11, 6));
		m.insert((11, 7), filter_map!(11, 7));
		m.insert((11, 8), filter_map!(11, 8));
		m.insert((11, 9), filter_map!(11, 9));
		m.insert((13, 10), filter_map!(13, 10));
		m.insert((13, 11), filter_map!(13, 11));
		m.insert((13, 12), filter_map!(13, 12));
		m.insert((13, 5), filter_map!(13, 5));
		m.insert((13, 6), filter_map!(13, 6));
		m.insert((13, 7), filter_map!(13, 7));
		m.insert((13, 8), filter_map!(13, 8));
		m.insert((13, 9), filter_map!(13, 9));
		m.insert((15, 10), filter_map!(15, 10));
		m.insert((15, 11), filter_map!(15, 11));
		m.insert((15, 12), filter_map!(15, 12));
		m.insert((15, 5), filter_map!(15, 5));
		m.insert((15, 6), filter_map!(15, 6));
		m.insert((15, 7), filter_map!(15, 7));
		m.insert((15, 8), filter_map!(15, 8));
		m.insert((15, 9), filter_map!(15, 9));
		m.insert((17, 10), filter_map!(17, 10));
		m.insert((17, 11), filter_map!(17, 11));
		m.insert((17, 12), filter_map!(17, 12));
		m.insert((17, 5), filter_map!(17, 5));
		m.insert((17, 6), filter_map!(17, 6));
		m.insert((17, 7), filter_map!(17, 7));
		m.insert((17, 8), filter_map!(17, 8));
		m.insert((17, 9), filter_map!(17, 9));
		m.insert((3, 5), filter_map!(3, 5));
		m.insert((3, 6), filter_map!(3, 6));
		m.insert((3, 7), filter_map!(3, 7));
		m.insert((3, 8), filter_map!(3, 8));
		m.insert((5, 10), filter_map!(5, 10));
		m.insert((5, 11), filter_map!(5, 11));
		m.insert((5, 12), filter_map!(5, 12));
		m.insert((5, 5), filter_map!(5, 5));
		m.insert((5, 6), filter_map!(5, 6));
		m.insert((5, 7), filter_map!(5, 7));
		m.insert((5, 8), filter_map!(5, 8));
		m.insert((5, 9), filter_map!(5, 9));
		m.insert((7, 10), filter_map!(7, 10));
		m.insert((7, 11), filter_map!(7, 11));
		m.insert((7, 12), filter_map!(7, 12));
		m.insert((7, 5), filter_map!(7, 5));
		m.insert((7, 6), filter_map!(7, 6));
		m.insert((7, 7), filter_map!(7, 7));
		m.insert((7, 8), filter_map!(7, 8));
		m.insert((7, 9), filter_map!(7, 9));
		m.insert((9, 10), filter_map!(9, 10));
		m.insert((9, 11), filter_map!(9, 11));
		m.insert((9, 12), filter_map!(9, 12));
		m.insert((9, 5), filter_map!(9, 5));
		m.insert((9, 6), filter_map!(9, 6));
		m.insert((9, 7), filter_map!(9, 7));
		m.insert((9, 8), filter_map!(9, 8));
		m.insert((9, 9), filter_map!(9, 9));

		m
	};
	pub static ref FILTER_SPECIFIC_MAP: std::collections::HashMap<(u8, u8), &'static [f64]> = {
		let mut m = std::collections::HashMap::new();

		m.insert((13, 8), filter_specific_map!(13, 8));
		m.insert((9, 7), filter_specific_map!(9, 7));
		m.insert((7, 9), filter_specific_map!(7, 9));
		m.insert((7, 5), filter_specific_map!(7, 5));
		m.insert((39, 8), filter_specific_map!(39, 8));
		m.insert((15, 6), filter_specific_map!(15, 6));
		m.insert((39, 6), filter_specific_map!(39, 6));
		m.insert((11, 11), filter_specific_map!(11, 11));
		m.insert((17, 11), filter_specific_map!(17, 11));
		m.insert((15, 8), filter_specific_map!(15, 8));
		m.insert((9, 5), filter_specific_map!(9, 5));
		m.insert((13, 6), filter_specific_map!(13, 6));
		m.insert((9, 9), filter_specific_map!(9, 9));
		m.insert((5, 11), filter_specific_map!(5, 11));
		m.insert((7, 7), filter_specific_map!(7, 7));
		m.insert((21, 6), filter_specific_map!(21, 6));
		m.insert((11, 6), filter_specific_map!(11, 6));
		m.insert((19, 11), filter_specific_map!(19, 11));
		m.insert((15, 11), filter_specific_map!(15, 11));
		m.insert((9, 11), filter_specific_map!(9, 11));
		m.insert((13, 11), filter_specific_map!(13, 11));
		m.insert((33, 10), filter_specific_map!(33, 10));
		m.insert((19, 6), filter_specific_map!(19, 6));
		m.insert((39, 10), filter_specific_map!(39, 10));
		m.insert((5, 6), filter_specific_map!(5, 6));
		m.insert((27, 12), filter_specific_map!(27, 12));
		m.insert((33, 6), filter_specific_map!(33, 6));
		m.insert((27, 8), filter_specific_map!(27, 8));
		m.insert((21, 12), filter_specific_map!(21, 12));
		m.insert((17, 8), filter_specific_map!(17, 8));
		m.insert((5, 8), filter_specific_map!(5, 8));
		m.insert((27, 6), filter_specific_map!(27, 6));
		m.insert((33, 8), filter_specific_map!(33, 8));
		m.insert((17, 6), filter_specific_map!(17, 6));
		m.insert((21, 10), filter_specific_map!(21, 10));
		m.insert((27, 10), filter_specific_map!(27, 10));
		m.insert((21, 8), filter_specific_map!(21, 8));
		m.insert((11, 8), filter_specific_map!(11, 8));
		m.insert((7, 11), filter_specific_map!(7, 11));
		m.insert((39, 12), filter_specific_map!(39, 12));
		m.insert((19, 8), filter_specific_map!(19, 8));
		m.insert((33, 12), filter_specific_map!(33, 12));
		m.insert((11, 12), filter_specific_map!(11, 12));
		m.insert((15, 7), filter_specific_map!(15, 7));
		m.insert((17, 12), filter_specific_map!(17, 12));
		m.insert((39, 9), filter_specific_map!(39, 9));
		m.insert((39, 5), filter_specific_map!(39, 5));
		m.insert((7, 8), filter_specific_map!(7, 8));
		m.insert((5, 12), filter_specific_map!(5, 12));
		m.insert((13, 5), filter_specific_map!(13, 5));
		m.insert((13, 9), filter_specific_map!(13, 9));
		m.insert((9, 6), filter_specific_map!(9, 6));
		m.insert((7, 6), filter_specific_map!(7, 6));
		m.insert((5, 10), filter_specific_map!(5, 10));
		m.insert((13, 7), filter_specific_map!(13, 7));
		m.insert((9, 8), filter_specific_map!(9, 8));
		m.insert((15, 5), filter_specific_map!(15, 5));
		m.insert((17, 10), filter_specific_map!(17, 10));
		m.insert((11, 10), filter_specific_map!(11, 10));
		m.insert((15, 9), filter_specific_map!(15, 9));
		m.insert((39, 7), filter_specific_map!(39, 7));
		m.insert((17, 9), filter_specific_map!(17, 9));
		m.insert((27, 9), filter_specific_map!(27, 9));
		m.insert((33, 7), filter_specific_map!(33, 7));
		m.insert((5, 7), filter_specific_map!(5, 7));
		m.insert((17, 5), filter_specific_map!(17, 5));
		m.insert((27, 5), filter_specific_map!(27, 5));
		m.insert((33, 11), filter_specific_map!(33, 11));
		m.insert((19, 7), filter_specific_map!(19, 7));
		m.insert((39, 11), filter_specific_map!(39, 11));
		m.insert((7, 12), filter_specific_map!(7, 12));
		m.insert((11, 7), filter_specific_map!(11, 7));
		m.insert((19, 10), filter_specific_map!(19, 10));
		m.insert((21, 7), filter_specific_map!(21, 7));
		m.insert((13, 10), filter_specific_map!(13, 10));
		m.insert((15, 10), filter_specific_map!(15, 10));
		m.insert((9, 10), filter_specific_map!(9, 10));
		m.insert((19, 5), filter_specific_map!(19, 5));
		m.insert((7, 10), filter_specific_map!(7, 10));
		m.insert((19, 9), filter_specific_map!(19, 9));
		m.insert((11, 9), filter_specific_map!(11, 9));
		m.insert((9, 12), filter_specific_map!(9, 12));
		m.insert((21, 9), filter_specific_map!(21, 9));
		m.insert((15, 12), filter_specific_map!(15, 12));
		m.insert((13, 12), filter_specific_map!(13, 12));
		m.insert((19, 12), filter_specific_map!(19, 12));
		m.insert((11, 5), filter_specific_map!(11, 5));
		m.insert((21, 5), filter_specific_map!(21, 5));
		m.insert((17, 7), filter_specific_map!(17, 7));
		m.insert((33, 9), filter_specific_map!(33, 9));
		m.insert((27, 7), filter_specific_map!(27, 7));
		m.insert((5, 9), filter_specific_map!(5, 9));
		m.insert((27, 11), filter_specific_map!(27, 11));
		m.insert((5, 5), filter_specific_map!(5, 5));
		m.insert((21, 11), filter_specific_map!(21, 11));
		m.insert((33, 5), filter_specific_map!(33, 5));
		m
	};
}

pub fn print_filters_list() {
	let mut filter_sizes = Vec::<u8>::new();
	let mut bit_sizes = Vec::<u8>::new();

	let mut combined_list = FILTER_MAP.iter().map(|((f, b), _)| (*f, *b)).collect::<HashSet<_>>();

	combined_list.extend(FILTER_SPECIFIC_MAP.iter().map(|((f, b), _)| (*f, *b)));

	let mut combined_list = combined_list.iter().cloned().collect_vec();

	let extra = combined_list
		.iter()
		.filter_map(|(f, b)| if *f * 2 < 39 { Some((f * 2, *b)) } else { None })
		.collect_vec();

	combined_list.extend(extra.iter());

	combined_list.sort_by(|a, b| {
		let init_cmp = a.1.cmp(&b.1);
		if init_cmp.is_eq() {
			a.0.cmp(&b.0)
		} else {
			init_cmp
		}
	});

	combined_list.iter().for_each(|(f, b)| {
		filter_sizes.push(*f);
		bit_sizes.push(*b);
	});

	print!("filter_sizes = [{}", filter_sizes[0]);

	for f in filter_sizes.iter().skip(1) {
		print!(", {}", f);
	}

	print!("]\n\n");

	print!("bit_sizes = [{}", bit_sizes[0]);

	for b in bit_sizes.iter().skip(1) {
		print!(", {}", b);
	}

	print!("]\n\n");

	println!("Number per model: {}", combined_list.len())
}
