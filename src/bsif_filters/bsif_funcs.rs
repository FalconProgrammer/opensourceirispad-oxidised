use cv::prelude::*;
use opencv as cv;

use anyhow::{bail, Result};
use itertools::Itertools;
use opencv::core::no_array;

/// Generate a BSIF histogram feature from an input image
///
/// # Arguments
///
/// * `image`: Image to process
/// * `filter_size`: BSIF filter size
/// * `bit_size`: BSIF bit size
///
/// returns: Result<Vec<i32, Global>, Error>
///
/// # Examples
///
/// ```no_run
/// use opencv::prelude::*;
/// use opensourceirispad_oxidised::bsif_filters::generate_histogram;
///
/// // This should be a loaded image
/// let image = Mat::default();
///
/// let histogram = generate_histogram(&image, 11, 10).unwrap();
/// ```
pub fn generate_histogram(image: &Mat, filter_size: u8, bit_size: u8) -> Result<Vec<i32>> {
	let code_image = generate_code_image(image, filter_size, bit_size)?;

	// Creating the histogram
	let mut histogram = vec![0; 2usize.pow(bit_size.into())];

	for (j, k) in (0..image.rows()).cartesian_product(0..image.cols()) {
		histogram[*code_image.at_2d::<f64>(j, k)? as usize - 1] += 1
	}

	Ok(histogram)
}

/// Generates an image of the BSIF histogram feature
///
/// # Arguments
///
/// * `image`: Image to process
/// * `filter_size`: BSIF filter size
/// * `bit_size`: BSIF bit size
///
/// returns: Result<Mat, Error>
///
/// # Examples
///
/// ```no_run
/// use opencv::prelude::*;
/// use opensourceirispad_oxidised::bsif_filters::generate_image;
///
/// // This should be a loaded image
/// let image = Mat::default();
///
/// let histogram = generate_image(&image, 11, 10).unwrap();
/// ```
pub fn generate_image(image: &Mat, filter_size: u8, bit_size: u8) -> Result<Mat> {
	let code_image = generate_code_image(image, filter_size, bit_size)?;

	let mut dst = unsafe { Mat::new_rows_cols(image.rows(), image.cols(), cv::core::CV_8UC1)? };
	cv::core::normalize(
		&code_image,
		&mut dst,
		0.,
		255.,
		cv::core::NORM_MINMAX,
		cv::core::CV_8UC1,
		&no_array(),
	)?;

	Ok(dst)
}

/// Generates the code image used to create the BSIF histogram
///
/// # Arguments
///
/// * `image`: Image to process
/// * `filter_size`: BSIF filter size
/// * `bit_size`: BSIF bit size
///
/// returns: Result<Mat, Error>
fn generate_code_image(image: &Mat, filter_size: u8, bit_size: u8) -> Result<Mat> {
	// Get hard-coded filter
	let my_filter = match super::const_filters::FILTER_MAP.get(&(filter_size, bit_size)) {
		Some(f) => f,
		None => match super::const_filters::FILTER_SPECIFIC_MAP.get(&(filter_size, bit_size)) {
			Some(f) => f,
			None => bail!(
				"Invalid Argument: no filter with filter_size {} and bit_size {}",
				filter_size,
				bit_size
			),
		},
	};

	//initializing matrix of 1s
	let mut code_image = Mat::ones(image.rows(), image.cols(), cv::core::CV_64FC1)?.to_mat()?;

	// creates the border around the image - it is wrapping
	let border = filter_size as i32 / 2;

	let mut image_wrap = Mat::default();
	cv::core::copy_make_border(
		&image,
		&mut image_wrap,
		border,
		border,
		border,
		border,
		cv::core::BORDER_WRAP,
		Default::default(),
	)?;

	// the textured image after filter
	let mut ci = Mat::default();
	let mut current_filter = unsafe { Mat::new_rows_cols(filter_size as i32, filter_size as i32, cv::core::CV_64FC1)? };

	for (filter_num, itr) in (0..bit_size).rev().zip(0..bit_size) {
		for row in 0..filter_size {
			for col in 0..filter_size {
				*current_filter.at_2d_mut::<f64>(row.into(), col.into())? = my_filter[s2i(
					filter_size as usize,
					bit_size as usize,
					row as usize,
					col as usize,
					filter_num as usize,
				)]
			}
		}

		// running the filter on the image w/ BORDER WRAP - equivalent to filter2 in matlab
		// filter2d will incidentally create another border - we do not want this extra border
		cv::imgproc::filter_2d(
			&image_wrap,
			&mut ci,
			cv::core::CV_64FC1,
			&current_filter,
			cv::core::Point::new(-1, -1),
			0.,
			cv::core::BORDER_CONSTANT,
		)?;

		// This will convert any positive values in the matrix
		// to 2^(i-1) as it did in the matlab software

		for (j, k) in (0..image.rows()).cartesian_product(0..image.cols()) {
			if *ci.at_2d::<f64>(j + border, k + border)? > 10.0_f64.powf(-3.0) {
				*code_image.at_2d_mut::<f64>(j, k)? = *code_image.at_2d::<f64>(j, k)? + 2.0_f64.powf(itr.into());
			}
		}
	}

	Ok(code_image)
}

/// Convert linear indexing to subscript indexing
fn s2i(filter_size: usize, bit_size: usize, row: usize, col: usize, bit: usize) -> usize {
	bit + bit_size * (col + filter_size * row)
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn s2i_tests() {
		assert_eq!(s2i(11, 10, 0, 0, 10 - 1), 9);
		assert_eq!(s2i(11, 10, 5, 3, 9), 589);
		assert_eq!(s2i(11, 10, 10, 10, 10 - 1), 1209);
	}
}
