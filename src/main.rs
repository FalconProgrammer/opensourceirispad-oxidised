use anyhow::{anyhow, Result};
use clap::Parser;
use itertools::{izip, Itertools};
use opencv::prelude::*;
use opensourceirispad_oxidised::config;
use opensourceirispad_oxidised::csv_helpers::load_csv_file_list;
use opensourceirispad_oxidised::feature_extractor::{
	extract_file_feature_histograms_to_vec, load_feature_histograms_from_hdf5, save_feature_histograms_to_hdf5,
};
use opensourceirispad_oxidised::model_details::{AllModelDetails, ModelDetails, ModelType, SerializableModelDetails};
use opensourceirispad_oxidised::pad_detector::{generate_model_filename, majority_vote, Model};
use std::collections::{HashMap, HashSet};
use std::io::Write;

use num_integer::Integer;

#[derive(Parser, Debug)]
#[clap(about, version)]
struct Args {
	#[clap(short, long)]
	output_filter_list: bool,
}

fn main() -> Result<()> {
	// NOTE (JB) Start up the logger
	simple_logger::SimpleLogger::new().init()?;

	let args = Args::parse();

	if args.output_filter_list {
		opensourceirispad_oxidised::bsif_filters::const_filters::print_filters_list();
		return Ok(());
	}

	// NOTE (JB) Load up the config
	let local_config = &config::CONFIG;

	let serial_model_details: SerializableModelDetails = local_config.into();

	let progress_style = indicatif::ProgressStyle::default_bar().template(
		"{msg} {spinner} {wide_bar:.cyan/blue} {pos}/{len} - {per_sec}/step - {elapsed_precise} / {duration_precise}",
	);

	let model_details = {
		let mut tmp_model_details = AllModelDetails::try_from(&serial_model_details)?;
		tmp_model_details.model_details = tmp_model_details
			.model_details
			.iter()
			.filter(|details| {
				if details.filter_size.is_odd()
					&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_MAP
						.contains_key(&(details.filter_size as u8, details.bit_size as u8))
					&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_SPECIFIC_MAP
						.contains_key(&(details.filter_size as u8, details.bit_size as u8))
					|| details.filter_size.is_even()
						&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_MAP
							.contains_key(&(details.filter_size as u8 / 2, details.bit_size as u8))
						&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_SPECIFIC_MAP
							.contains_key(&(details.filter_size as u8 / 2, details.bit_size as u8))
				{
					println!(
						"Unable to find filter/bit combination {} {} in const filters. Removing.",
						details.filter_size, details.bit_size
					);
					false
				} else {
					true
				}
			})
			.cloned()
			.collect();

		tmp_model_details
	};

	// NOTE (JB) In order to not process multiple filter/bit size combinations, we create a unique set
	let unique_filter_bit_combinations = model_details
		.model_details
		.iter()
		.map(|m| (m.filter_size, m.bit_size))
		.filter(|(filter, bit)| {
			if filter.is_odd()
				&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_MAP
					.contains_key(&(*filter as u8, *bit as u8))
				&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_SPECIFIC_MAP
					.contains_key(&(*filter as u8, *bit as u8))
				|| filter.is_even()
					&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_MAP
						.contains_key(&(*filter as u8 / 2, *bit as u8))
					&& !opensourceirispad_oxidised::bsif_filters::const_filters::FILTER_SPECIFIC_MAP
						.contains_key(&(*filter as u8 / 2, *bit as u8))
			{
				println!(
					"Unable to find filter/bit combination {} {} in const filters. Removing.",
					filter, bit
				);
				false
			} else {
				true
			}
		})
		.sorted_by(|a, b| {
			let init_cmp = a.1.cmp(&b.1);
			if init_cmp.is_eq() {
				a.0.cmp(&b.0)
			} else {
				init_cmp
			}
		})
		.collect::<HashSet<_>>();

	// NOTE (JB) Load up the training/testing lists
	let train_set = load_csv_file_list(&local_config.training.train_set_list)?;
	let test_set = load_csv_file_list(&local_config.evaluation.test_set_list)?;

	// NOTE (JB) These will be used to store the features from the next stages
	let mut extracted_train_features = HashMap::<(i32, i32), Vec<(String, Vec<i32>)>>::new();
	let mut extracted_test_features = HashMap::<(i32, i32), Vec<(String, Vec<i32>)>>::new();

	// NOTE (JB) Extract out the file list from the train/set
	let train_file_list = train_set.iter().map(|(file, _)| file.clone()).collect::<Vec<_>>();
	let test_file_list = test_set.iter().map(|(file, _)| file.clone()).collect::<Vec<_>>();

	// NOTE (JB) If need to extract features (if we're saving, or training/testing and need to extract
	if local_config.feature_extraction.extract_to_file
		|| (local_config.training.train && !local_config.training.load_features_from_file)
		|| (local_config.evaluation.test && !local_config.evaluation.load_features_from_file)
	{
		// NOTE (JB) Get the output directory to save to if we are doing so
		let save = if local_config.feature_extraction.extract_to_file {
			Some(local_config.feature_extraction.output_directory.to_string())
		} else {
			None
		};

		// NOTE (JB) Combine the file lists
		// TODO (JB) Determine whether train/test needs to be extracted
		let mut all_file_list = train_file_list.clone();
		all_file_list.extend(test_file_list.iter().cloned());

		let mut all_file_list = all_file_list.into_iter().unique().collect();

		// NOTE (JB) Define if we need to get out the histograms
		let return_features =
			!local_config.training.load_features_from_file || !local_config.evaluation.load_features_from_file;

		// NOTE (JB) Extract/save histogram features
		let extracted_features = extract_save_histograms(
			&all_file_list,
			&unique_filter_bit_combinations,
			return_features,
			save,
			Some(&progress_style),
		)?;

		// NOTE (JB) If we're not loading from file for training
		if !local_config.training.load_features_from_file {
			extracted_train_features.extend(extracted_features.iter().map(|(key, features)| {
				let train_features = features
					.iter()
					.filter(|(name, _)| train_file_list.contains(name))
					.cloned()
					.collect::<Vec<_>>();

				((*key).clone(), train_features)
			}));
		}

		// NOTE (JB) If we're not loading from file for testing
		if !local_config.evaluation.load_features_from_file {
			extracted_test_features.extend(extracted_features.iter().map(|(key, features)| {
				let test_features = features
					.iter()
					.filter(|(name, _)| test_file_list.contains(name))
					.cloned()
					.collect::<Vec<_>>();

				((*key).clone(), test_features)
			}));
		}
	}

	if local_config.training.train {
		let train_set_labels = train_set.iter().cloned().collect::<HashMap<_, _>>();
		let pb = indicatif::ProgressBar::new(model_details.model_details.len() as u64);
		pb.set_style(progress_style.clone());
		pb.enable_steady_tick(500);

		let model_output_dir = std::path::Path::new(local_config.training.output_directory.as_ref());

		if !model_output_dir.exists() {
			std::fs::create_dir_all(model_output_dir)?;
		}

		for details in &model_details.model_details {
			if unique_filter_bit_combinations.contains(&(details.filter_size, details.bit_size)) {
				pb.set_message(format!(
					"Training model [ {} ] on filter {} bit {}:",
					&details.model_type, &details.filter_size, &details.bit_size
				));

				let model_file = opensourceirispad_oxidised::pad_detector::generate_model_filename(
					details.model_type.clone(),
					details.filter_size,
					details.bit_size,
				);

				let model_full_path = model_output_dir.join(&model_file);
				let model_full_path_str = model_full_path.to_str().unwrap();

				if model_full_path.exists() {
					pb.inc(1);
					continue;
				}

				let model = if local_config.training.load_features_from_file {
					let loaded_features = load_filter_from_disk(
						details.filter_size,
						details.bit_size,
						local_config.feature_extraction.file_prefix.as_ref(),
						local_config.feature_extraction.output_directory.as_ref(),
					)?;

					let train_file_list_under = train_file_list.iter().map(|f| f.replace("/", "_")).collect::<Vec<_>>();

					let ref_features = extract_files_from_filters(&loaded_features, &train_file_list_under);

					// NOTE (JB) Train the model
					let model = opensourceirispad_oxidised::pad_detector::train_model_ref_vec(
						&ref_features,
						&train_set_labels,
						details,
					)?;

					model
				} else {
					let features_with_filename = extracted_train_features
						.get(&(details.filter_size, details.bit_size))
						.ok_or(anyhow!(
							"Unable to find filter {} bit_size {} in extracted features",
							&details.filter_size,
							&details.bit_size
						))?;

					// NOTE (JB) Train the model
					let model = opensourceirispad_oxidised::pad_detector::train_model(
						&features_with_filename,
						&train_set_labels,
						details,
					)?;

					model
				};

				// NOTE (JB) Save the Model
				model.save_model(model_full_path_str)?;

				pb.inc(1);
			}
		}
	}

	if local_config.evaluation.test {
		// NOTE (JB) Cycle through each model to get individual results
		let test_set_labels = test_set.iter().cloned().collect::<HashMap<_, _>>();
		let mut overall_results = Vec::<(Mat, &ModelDetails)>::new();
		let mut test_vec_labels = Vec::<i32>::new();

		if !local_config.evaluation.load_features_from_file {
			if extracted_test_features.is_empty() {
				anyhow::bail!("Config specifies to not load features from file for evaluation, however no features extracted in memory. Aborting.");
			}
		}

		let pb = indicatif::ProgressBar::new(model_details.model_details.len() as u64);
		pb.set_style(progress_style.clone());
		pb.enable_steady_tick(500);

		for details in &model_details.model_details {
			if unique_filter_bit_combinations.contains(&(details.filter_size, details.bit_size)) {
				pb.set_message(format!(
					"Testing model [ {} ] on filter {} bit {}:",
					&details.model_type, &details.filter_size, &details.bit_size
				));

				let model_filename = opensourceirispad_oxidised::pad_detector::generate_model_filename(
					details.model_type.clone(),
					details.filter_size,
					details.bit_size,
				);
				let full_model_path = std::path::Path::new(local_config.model_info.model_directory.as_ref());
				let full_model_path = full_model_path.join(model_filename);

				if !full_model_path.exists() {
					println!(
						"Evaluation error: model [ {} ] does not exist",
						full_model_path.to_str().unwrap()
					);
					pb.inc(1);
					continue;
				}

				// NOTE (JB) Load model
				let model = opensourceirispad_oxidised::pad_detector::Model::load_model(
					full_model_path.to_str().unwrap(),
					&details.model_type,
				)?;

				let features_with_filename = if local_config.evaluation.load_features_from_file {
					let loaded_features = load_filter_from_disk(
						details.filter_size,
						details.bit_size,
						local_config.feature_extraction.file_prefix.as_ref(),
						local_config.feature_extraction.output_directory.as_ref(),
					)?;

					let test_file_list_under = test_file_list.iter().map(|f| f.replace("/", "_")).collect::<Vec<_>>();

					let ref_features = extract_files_from_filters(&loaded_features, &test_file_list_under);

					if ref_features.is_empty() {
						anyhow::bail!("No features in testing set from [ {} ] can be found in features file [ {}/{} ]. Do you have the right config setup?", local_config.evaluation.test_set_list.as_ref(), local_config.feature_extraction.output_directory.as_ref(), determine_filter_filename(details.filter_size, details.bit_size, local_config.feature_extraction.file_prefix.as_ref()));
					}

					ref_features.iter().cloned().cloned().collect_vec()
				} else {
					// NOTE (JB) Get features
					let a = extracted_test_features
						.get(&(details.filter_size, details.bit_size))
						.ok_or(anyhow!(
							"Unable to find filter {} bit_size {} in extracted features",
							&details.filter_size,
							&details.bit_size
						))?
						.clone();

					a
				};

				let (cv_samples, cv_responses_res) =
					opensourceirispad_oxidised::pad_detector::convert_feature_set_to_cv(
						&features_with_filename,
						Some(&test_set_labels),
						details.bit_size,
					)?;
				let cv_responses = cv_responses_res.unwrap();

				if test_vec_labels.is_empty() {
					test_vec_labels = cv_responses.iter::<i32>()?.map(|(_, v)| v).collect_vec();
				}

				match &model {
					Model::SVM(model) => {
						let mut model_results = unsafe {
							Mat::new_rows_cols(cv_responses.rows(), cv_responses.cols(), opencv::core::CV_32FC1)?
						};

						model.predict(&cv_samples, &mut model_results, 0)?;

						overall_results.push((model_results, details));
					}
					Model::RF(model) => {
						let mut model_results = unsafe {
							Mat::new_rows_cols(cv_responses.rows(), cv_responses.cols(), opencv::core::CV_32FC1)?
						};

						model.predict(&cv_samples, &mut model_results, 0)?;

						overall_results.push((model_results, details));
					}
					Model::MP(model) => {
						let mut model_results =
							unsafe { Mat::new_rows_cols(cv_responses.rows(), 2, opencv::core::CV_32FC1)? };

						model.predict(&cv_samples, &mut model_results, 0)?;

						let mut model_final_results =
							unsafe { Mat::new_rows_cols(cv_responses.rows(), 1, opencv::core::CV_32FC1)? };

						for i in 0..cv_responses.rows() {
							let res_0 = model_results.at_2d::<f32>(i, 0)?;
							let res_1 = model_results.at_2d::<f32>(i, 1)?;

							let final_result =
								opensourceirispad_oxidised::pad_detector::nn_response_to_label(res_0, res_1);

							*model_final_results.at_mut(i)? = final_result;
						}

						overall_results.push((model_results, details));
					}
				}
			}

			pb.inc(1);
		}

		println!("Finished Evaluating Models. Total results: {}", overall_results.len());

		// NOTE (JB) Test model (and store)
		let output_file = std::fs::File::create(local_config.evaluation.classifications_filename.as_ref())?;
		let mut output_buffer = std::io::BufWriter::new(output_file);

		if local_config.model_info.majority_voting {
			let mut overall_votes = Vec::<i32>::new();

			for t in 0..test_set.len() as i32 {
				let mut in_favour = 0;
				let mut against = 0;

				for (m_res, _) in &overall_results {
					if *m_res.at::<f32>(t)? > 0.5 {
						in_favour += 1;
					} else {
						against += 1;
					}
				}

				overall_votes.push(if in_favour >= against { 1 } else { 0 });
			}

			add_to_classification_file(&overall_votes, &test_vec_labels, &mut output_buffer);

			println!(
				"Number of models in the ensemble: {}",
				model_details.model_details.len()
			);

			let stats = BiometricStats::new_from_results(&overall_votes, &test_vec_labels);
			println!("{}", stats);
		} else {
			for (res, details) in overall_results.iter() {
				let current_result = res
					.iter::<f32>()?
					.map(|(_p, v)| if v >= 0.5 { 1 } else { 0 })
					.collect_vec();

				write!(
					output_buffer,
					"{},{},{},\n",
					details.model_type, details.filter_size, details.bit_size
				)?;
				add_to_classification_file(&current_result, &test_vec_labels, &mut output_buffer);
				write!(output_buffer, "--------------------")?;

				let stats = BiometricStats::new_from_results(&current_result, &test_vec_labels);
				println!(
					"Model: {}\n{}",
					generate_model_filename(details.model_type.clone(), details.filter_size, details.bit_size),
					stats
				);
			}
		}

		drop(output_buffer);

		// NOTE (JB) Perform Model Analysis for best Ensemble generation, sorted with maximum CCR first
		let all_stats = overall_results
			.iter()
			.map(|(res, details)| {
				let current_result = res
					.iter::<f32>()
					.unwrap()
					.map(|(_p, v)| if v >= 0.5 { 1 } else { 0 })
					.collect_vec();

				(
					*details,
					BiometricStats::new_from_results(&current_result, &test_vec_labels),
					current_result,
				)
			})
			.sorted_by(|(_d_0, s_0, _r_0), (_d_1, s_1, _r_1)| s_1.ccr.partial_cmp(&s_0.ccr).unwrap())
			.collect_vec();

		// NOTE (JB) Find the best ensemble with the highest CCR
		let mut best_comb = 0;
		let mut best_stats = BiometricStats::default();

		let all_n_file = std::fs::File::create("all_n_stats.csv").unwrap();
		let mut all_n_buffer = std::io::BufWriter::new(all_n_file);

		writeln!(&mut all_n_buffer, "CCR, ACPER, BPCER,").unwrap();

		for comb_num in 0..all_stats.len() {
			let current_results = all_stats
				.iter()
				.map(|(_, _, r)| r)
				.take(comb_num + 1)
				.cloned()
				.collect_vec();
			let current_voted_results = majority_vote(&current_results)?;
			let current_stats = BiometricStats::new_from_results(&current_voted_results, &test_vec_labels);

			writeln!(&mut all_n_buffer, "{}, {}, {},", current_stats.ccr, current_stats.apcer, current_stats.bpcer).unwrap();

			if current_stats.ccr >= best_stats.ccr {
				best_stats = current_stats;
				best_comb = comb_num;
			}
		}

		all_n_buffer.flush().unwrap();

		// NOTE (JB) Collate the best combination model details
		let best_collated_details = all_stats
			.iter()
			.map(|(d, _, _)| *d)
			.take(best_comb + 1)
			.cloned()
			.collect_vec();

		println!("Best combination contains:");
		println!(
			"\t{} models in total, {} SVMs, {} RFs, {} MPs",
			best_collated_details.len(),
			best_collated_details
				.iter()
				.filter(|d| d.model_type == ModelType::SVM)
				.count(),
			best_collated_details
				.iter()
				.filter(|d| d.model_type == ModelType::RF)
				.count(),
			best_collated_details
				.iter()
				.filter(|d| d.model_type == ModelType::MP)
				.count(),
		);
		println!("Final Stats:\n{}", &best_stats);

		// NOTE (JB) Serialize to TOML
		let best_model_details = AllModelDetails {
			model_details: best_collated_details,
			majority_voting: true,
			model_directory: model_details.model_directory.clone(),
		};

		let best_model_details_serialized = SerializableModelDetails::from(&best_model_details);
		let toml_params_string = toml::to_string_pretty(&best_model_details_serialized)?;

		// NOTE (JB) Output to file
		let mut best_models_file = std::fs::File::create("best_model_details.toml")?;
		write!(best_models_file, "{}", toml_params_string)?;

		println!("\nOutput to [ best_model_details.toml ]");
	}

	Ok(())
}

fn add_to_classification_file(
	overall_results: &Vec<i32>,
	testing_labels: &Vec<i32>,
	writer: &mut std::io::BufWriter<std::fs::File>,
) {
	for (lbl, res) in izip!(testing_labels, overall_results) {
		writer.write(format!("{},{}\n", lbl, res).as_bytes()).unwrap();
	}
}

#[derive(Default, Copy, Clone)]
struct BiometricStats {
	pub ccr: f32,
	pub apcer: f32,
	pub bpcer: f32,
}

impl std::fmt::Display for BiometricStats {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"CCR: {:.3}%\nAPCER: {:.3}%\nBPCER: {:.3}%\n",
			self.ccr * 100.,
			self.apcer * 100.,
			self.bpcer * 100.
		)
	}
}

impl BiometricStats {
	/// Calculates CCR, APCER, and BPCER.
	///
	/// # Arguments
	///
	/// * `overall_results`: predicted label - either 0 or 1
	/// * `testing_labels`: testing label - either 0 or 1
	///
	/// returns: BiometricStats
	pub fn new_from_results(overall_results: &Vec<i32>, testing_labels: &Vec<i32>) -> BiometricStats {
		let mut num_incorrect = 0;
		let mut ap_count = 0;
		let mut bp_count = 0;
		let mut num_bonafide = 0;
		let mut num_attack = 0;

		// NOTE (JB) Calculate the number of bonafide/attacks in the testing set
		for i in testing_labels {
			if *i == 0 {
				num_bonafide += 1;
			} else {
				num_attack += 1
			}
		}

		// NOTE (JB) Count the incorrect predictions
		for (pred, act) in izip!(overall_results, testing_labels) {
			if *pred != *act {
				num_incorrect += 1;

				if *act == 0 {
					bp_count += 1;
				} else {
					ap_count += 1;
				}
			}
		}

		// NOTE (JB) Calculate the final statistics
		let ccr = 1.0 - (num_incorrect as f32 / testing_labels.len() as f32);
		let apcer = ap_count as f32 / num_attack as f32;
		let bpcer = bp_count as f32 / num_bonafide as f32;

		BiometricStats { ccr, apcer, bpcer }
	}
}

fn extract_save_histograms(
	file_list: &Vec<String>,
	unique_filter_bit_combinations: &HashSet<(i32, i32)>,
	return_features: bool,
	save_folder: Option<String>,
	pb_style: Option<&indicatif::ProgressStyle>,
) -> Result<HashMap<(i32, i32), Vec<(String, Vec<i32>)>>> {
	let pb = indicatif::ProgressBar::new(unique_filter_bit_combinations.len() as u64);

	if let Some(pb_style) = pb_style {
		pb.set_style(pb_style.clone());
	}

	pb.enable_steady_tick(500);

	let mut all_features = HashMap::new();

	for (filter_size, bit_size) in unique_filter_bit_combinations.iter() {
		pb.set_message(format!("Extracting Filter/Size ({}, {})", filter_size, bit_size));

		let (down_sample, act_filter_size) = if filter_size % 2 == 0 {
			(true, filter_size / 2)
		} else {
			(false, *filter_size)
		};

		if let Some(save_folder) = &save_folder {
			let file_name = format!(
				"{}_filter_{}_{}_{}.hdf5",
				config::CONFIG.feature_extraction.file_prefix,
				filter_size,
				filter_size,
				bit_size
			);
			let file_path = std::path::Path::new(save_folder).join(&file_name);

			if file_path.exists() {
				pb.inc(1);
				continue;
			}
		}

		let histograms =
			extract_file_feature_histograms_to_vec(file_list, act_filter_size as u8, *bit_size as u8, down_sample)
				.into_iter()
				.map(|(name, hist)| (name.replace("/", "_"), hist))
				.collect::<Vec<_>>();

		if let Some(save_folder) = &save_folder {
			let file_name = format!(
				"{}_filter_{}_{}_{}.hdf5",
				config::CONFIG.feature_extraction.file_prefix,
				filter_size,
				filter_size,
				bit_size
			);
			let file_path = std::path::Path::new(save_folder).join(&file_name);

			save_feature_histograms_to_hdf5(
				&histograms,
				file_path.to_str().expect("Unable to convert path to string???!!!"),
			)?;
		}

		if return_features {
			all_features.insert((*filter_size, *bit_size), histograms);
		}

		pb.inc(1);
	}

	pb.set_message("Extracting Filter/Size");
	pb.finish();

	Ok(all_features)
}

fn load_filter_from_disk(
	filter_size: i32,
	bit_size: i32,
	file_prefix: &str,
	feature_directory: &str,
) -> Result<Vec<(String, Vec<i32>)>> {
	// NOTE (JB) Form file name
	// TODO (JB) Extract out into separate function for here and extract function below
	let file_name = determine_filter_filename(filter_size, bit_size, file_prefix);
	let file_path = std::path::Path::new(feature_directory).join(&file_name);

	// NOTE (JB) Load the features from file
	return Ok(load_feature_histograms_from_hdf5(
		file_path.to_str().expect("Unable to convert path to string???!!!"),
	)?);
}

fn determine_filter_filename(filter_size: i32, bit_size: i32, file_prefix: &str) -> String {
	format!(
		"{}_filter_{}_{}_{}.hdf5",
		file_prefix, filter_size, filter_size, bit_size
	)
}

fn extract_files_from_filters<'a>(
	filters: &'a Vec<(String, Vec<i32>)>,
	file_list_underscored: &Vec<String>,
) -> Vec<&'a (String, Vec<i32>)> {
	use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

	// NOTE (JB) Get training features
	return filters
		.par_iter()
		.filter(|(key, _features)| file_list_underscored.contains(key))
		.collect();
}
