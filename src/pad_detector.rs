use crate::model_details::{AllModelDetails, ModelDetails, ModelType, SerializableModelDetails};
use anyhow::{anyhow, bail, Result};
use itertools::Itertools;
use log::*;
use opencv::core::no_array;
use opencv::prelude::*;
use rayon::prelude::*;
use std::collections::HashMap;

/// Contains one of SVM/Random Forest/Multi-Layer Perceptron models with common functionality
pub enum Model {
	SVM(opencv::core::Ptr<dyn opencv::ml::SVM>),
	RF(opencv::core::Ptr<dyn opencv::ml::RTrees>),
	MP(opencv::core::Ptr<dyn opencv::ml::ANN_MLP>),
}

unsafe impl Send for Model {}
unsafe impl Sync for Model {}

struct ThreadMat(Mat);

unsafe impl Send for ThreadMat {}
unsafe impl Sync for ThreadMat {}

impl Model {
	/// Gets the Model Type
	pub fn get_model_type(&self) -> ModelType {
		match self {
			Model::SVM(_) => ModelType::SVM,
			Model::RF(_) => ModelType::RF,
			Model::MP(_) => ModelType::MP,
		}
	}

	/// Saves any model
	///
	/// # Arguments
	///
	/// * `filename`: output filename of the model
	///
	/// returns: Result<(), Error>
	pub fn save_model(&self, filename: &str) -> anyhow::Result<()> {
		use opencv::prelude::*;

		match self {
			Model::SVM(svm) => {
				svm.save(filename)?;
			}
			Model::RF(rf) => {
				rf.save(filename)?;
			}
			Model::MP(mp) => {
				mp.save(filename)?;
			}
		}

		Ok(())
	}

	/// Loads a singular model from file
	///
	/// # Arguments
	///
	/// * `filename`: path to the full model
	/// * `model_type`: the type of model
	///
	/// returns: Result<Model, Error>
	pub fn load_model(filename: &str, model_type: &ModelType) -> anyhow::Result<Model> {
		Ok(match model_type {
			ModelType::SVM => Model::SVM(<dyn opencv::ml::SVM>::load(filename)?),
			ModelType::RF => Model::RF(<dyn opencv::ml::RTrees>::load(filename, "")?),
			ModelType::MP => Model::MP(<dyn opencv::ml::ANN_MLP>::load(filename)?),
		})
	}

	/// Predict a presentation attack (1) or bonafide (0) using majority voting of all loaded models
	///
	/// # Arguments
	///
	/// * `input_iris_image`: A Vec of iris images to predict
	/// * `filter_size`: The BSIF filter size
	/// * `bit_size`: The BSIF bit size
	///
	/// returns: Result<Vec<i32, Global>, Error>
	pub fn predict(&self, input_iris_image: &Vec<Mat>, filter_size: u8, bit_size: u8) -> anyhow::Result<Vec<i32>> {
		// NOTE (JB) Extract Features
		let extracted_features = input_iris_image
			.iter()
			.map(|image| crate::feature_extractor::extract_mat_feature_histograms(image, filter_size, bit_size))
			.collect::<Result<Vec<Vec<i32>>, anyhow::Error>>()?;

		// NOTE (JB) Convert to OpenCV
		let features_mat = Mat::zeros(
			extracted_features.len() as i32,
			2_i32.pow(bit_size as u32),
			opencv::core::CV_32FC1,
		)?
		.to_mat()?;

		for (f, feature) in extracted_features.iter().enumerate() {
			// NOTE (JB) Copy features
			let mut features_row = features_mat.row(f as i32)?;

			feature_to_cv_ref(feature, &mut features_row)?;
		}

		self.predict_from_extracted_features(&features_mat)
	}

	/// Predicts result from a pre-extracted BSIF histogram features mat
	///
	/// # Arguments
	///
	/// * `features_mat`: An OpenCV Mat with a set of histogram features for each row
	///
	/// returns: Result<Vec<i32, Global>, Error>
	pub fn predict_from_extracted_features(&self, features_mat: &Mat) -> Result<Vec<i32>> {
		// NOTE (JB) Run Model
		let model_results = match self {
			Model::SVM(model) => {
				let mut model_results = unsafe { Mat::new_rows_cols(features_mat.rows(), 1, opencv::core::CV_32FC1)? };

				model.predict(&features_mat, &mut model_results, 0)?;

				model_results
			}
			Model::RF(model) => {
				let mut model_results = unsafe { Mat::new_rows_cols(features_mat.rows(), 1, opencv::core::CV_32FC1)? };

				model.predict(&features_mat, &mut model_results, 0)?;

				model_results
			}
			Model::MP(model) => {
				let mut model_results = unsafe { Mat::new_rows_cols(features_mat.rows(), 2, opencv::core::CV_32FC1)? };

				model.predict(&features_mat, &mut model_results, 0)?;

				let mut model_final_results =
					unsafe { Mat::new_rows_cols(features_mat.rows(), 1, opencv::core::CV_32FC1)? };

				for i in 0..features_mat.rows() {
					let res_0 = model_results.at_2d::<f32>(i, 0)?;
					let res_1 = model_results.at_2d::<f32>(i, 1)?;

					let final_result = nn_response_to_label(res_0, res_1);

					*model_final_results.at_mut(i)? = final_result;
				}

				model_results
			}
		};

		// NOTE (JB) Extract Predictions
		Ok(model_results
			.iter::<f32>()?
			.map(|(_p, v)| if v >= 0.5 { 1 } else { 0 })
			.collect_vec())
	}
}

/// Handles all the loading of models, and predictions
pub struct IrisPADModel {
	models: Vec<(ModelDetails, Model)>,
}

impl IrisPADModel {
	/// Try to load up all the models from an AllModelDetails struct
	///
	/// # Arguments
	///
	/// * `model_details`:
	///
	/// returns: Result<IrisPADModel, Error>
	pub fn try_new_from_details(model_details: &AllModelDetails) -> Result<IrisPADModel> {
		let models = model_details
			.model_details
			.iter()
			.filter_map(|details| {
				let model_filename = generate_model_filename(details.model_type, details.filter_size, details.bit_size);
				let full_model_path = std::path::Path::new(&model_details.model_directory);
				let full_model_path = full_model_path.join(model_filename);

				if !full_model_path.exists() {
					warn!(
						"Loading Iris PAD Models: model with path [ {} ] does not exist",
						full_model_path.to_str().unwrap()
					);

					None
				} else {
					let model = Model::load_model(full_model_path.to_str().unwrap(), &details.model_type);

					match model {
						Ok(m) => Some((details.clone(), m)),
						Err(e) => {
							warn!(
								"Loading Iris PAD Models: model with path [ {} ] has error: {}",
								full_model_path.to_str().unwrap(),
								e
							);
							None
						}
					}
				}
			})
			.collect::<Vec<_>>();

		info!(
			"Loading Iris PAD Models: Loaded {}/{} models",
			models.len(),
			model_details.model_details.len()
		);

		if models.len() == 0 {
			bail!("Loading Iris PAD Models: No models able to load")
		} else {
			Ok(IrisPADModel { models })
		}
	}

	/// Calls @try_new_from_details after attempting to parse a toml string.
	///
	/// # Arguments
	///
	/// * `model_details`: String containing toml formatted model information.
	///
	/// returns: Result<IrisPADModel, Error>
	pub fn try_new_from_toml_details(model_details: &str) -> Result<IrisPADModel> {
		let serializable_res = toml::from_str::<SerializableModelDetails>(model_details);

		let serializable = match serializable_res {
			Ok(s) => s,
			Err(e) => {
				bail!("Unable to parse toml model string: {}", e);
			}
		};

		let parsed = AllModelDetails::try_from(&serializable)?;

		IrisPADModel::try_new_from_details(&parsed)
	}

	pub fn try_new_from_toml_file(model_file: &str) -> Result<IrisPADModel> {
		let toml_string = std::fs::read_to_string(model_file);

		match &toml_string {
			Ok(s) => IrisPADModel::try_new_from_toml_details(s),
			Err(e) => {
				bail!("Unable to open toml file: {}", e);
			}
		}
	}

	/// Predicts presentation attack (1) or bonafide (0) for all provided images based on a majority
	/// vote of the loaded models
	///
	/// # Arguments
	///
	/// * `iris_images`:
	///
	/// returns: Result<Vec<i32, Global>, Error>
	pub fn predict(&self, iris_images: &Vec<Mat>) -> Result<Vec<i32>> {
		let thread_mats = iris_images.iter().map(|i| ThreadMat(i.clone())).collect_vec();

		let model_results = self
			.models
			.par_iter()
			.map(|(details, model)| {
				let mats = thread_mats.iter().map(|i| i.0.clone()).collect_vec();

				model.predict(&mats, details.filter_size as u8, details.bit_size as u8)
			})
			.collect::<Result<Vec<_>>>()?;

		majority_vote(&model_results)
	}

	/// Predicts presentation attack (1) or bonafide (0) for all provided images based on a majority
	/// vote of the loaded models
	///
	/// # Arguments
	///
	/// * `iris_images`:
	///
	/// returns: Result<Vec<f32, Global>, Error>
	pub fn predict_float(&self, iris_images: &Vec<Mat>) -> Result<Vec<f32>> {
		let thread_mats = iris_images.iter().map(|i| ThreadMat(i.clone())).collect_vec();

		let model_results = self
			.models
			.par_iter()
			.map(|(details, model)| {
				let mats = thread_mats.iter().map(|i| i.0.clone()).collect_vec();

				model.predict(&mats, details.filter_size as u8, details.bit_size as u8)
			})
			.collect::<Result<Vec<_>>>()?;

		majority_vote_float(&model_results)
	}
}

/// Collects together the majority vote of whether it's a textured contact (1) or not (0)
///
/// # Arguments
///
/// * `results`: Vec of results - all internal vecs must by of same length otherwise will panic
///
/// returns: Vec<i32, Global>
pub fn majority_vote(results: &Vec<Vec<i32>>) -> Result<Vec<i32>> {
	let mut overall_votes = Vec::<i32>::new();

	anyhow::ensure!(
		results.iter().all(|res| { res.len() == results[0].len() }),
		"Majority Vote: Input results have inconsistent results: {:?}",
		results.iter().map(|res| { res.len() }).collect_vec()
	);

	for t in 0..results[0].len() {
		let mut in_favour = 0;
		let mut against = 0;

		for m_res in results {
			if m_res[t] == 1 {
				in_favour += 1;
			} else {
				against += 1;
			}
		}

		overall_votes.push(if in_favour >= against { 1 } else { 0 });
	}

	Ok(overall_votes)
}

/// Collects together the majority vote of whether it's a textured contact (1) or not (0)
///
/// # Arguments
///
/// * `results`:
///
/// returns: Result<Vec<f32, Global>, Error>
///
/// # Examples
///
/// ```
///
/// ```
pub fn majority_vote_float(results: &Vec<Vec<i32>>) -> Result<Vec<f32>> {
	let mut overall_votes = Vec::<f32>::new();

	anyhow::ensure!(
		results.iter().all(|res| { res.len() == results[0].len() }),
		"Majority Vote: Input results have inconsistent results: {:?}",
		results.iter().map(|res| { res.len() }).collect_vec()
	);

	for t in 0..results[0].len() {
		let mut in_favour = 0;
		let mut against = 0;

		assert_eq!(results[0].len(), results[t].len());

		for m_res in results {
			if m_res[t] == 1 {
				in_favour += 1;
			} else {
				against += 1;
			}
		}

		overall_votes.push(in_favour as f32 / (in_favour + against) as f32);
	}

	Ok(overall_votes)
}

/// Trains a Iris PAD model from pre-extracted histogram features
///
/// # Arguments
///
/// * `train_features`: A vec of containing extracted histograms and their filename (should have '/' replaced with '_')
/// * `train_labels_map`: A map of training exemplar filename to their label. Label should be 0 or 1
/// * `details`: The details required to train the model
///
/// returns: Result<Model, Error>
pub fn train_model(
	train_features: &Vec<(String, Vec<i32>)>,
	train_labels_map: &HashMap<String, i32>,
	details: &ModelDetails,
) -> Result<Model> {
	let train_features_ref_vec = train_features.iter().collect_vec();

	return train_model_ref_vec(&train_features_ref_vec, train_labels_map, details);
}

pub fn train_model_ref_vec(
	train_features: &Vec<&(String, Vec<i32>)>,
	train_labels_map: &HashMap<String, i32>,
	details: &ModelDetails,
) -> Result<Model> {
	use opencv::ml;

	// NOTE (JB) Convert input vectors to opencv mat
	let (train_set, train_labels) =
		convert_feature_set_to_cv_ref_vec(&train_features, Some(&train_labels_map), details.bit_size)?;

	let train_labels = train_labels.unwrap();

	let cv_train_data = <dyn opencv::ml::TrainData>::create(
		&train_set,
		opencv::ml::ROW_SAMPLE,
		&train_labels,
		&no_array(),
		&no_array(),
		&no_array(),
		&no_array(),
	)?;

	// NOTE (JB) Train models
	let model = match &details.model_type {
		ModelType::SVM => {
			let mut svm_model = <dyn ml::SVM>::create()?;

			svm_model.set_type(ml::SVM_Types::C_SVC as i32)?;
			svm_model.set_kernel(ml::SVM_KernelTypes::RBF as i32)?;

			// SVM Default Arguments
			let default_c = <dyn ml::SVM>::get_default_grid(ml::SVM_ParamTypes::C as i32)?;
			let default_gamma = <dyn ml::SVM>::get_default_grid(ml::SVM_ParamTypes::GAMMA as i32)?;
			let default_p = <dyn ml::SVM>::get_default_grid(ml::SVM_ParamTypes::P as i32)?;
			let default_nu = <dyn ml::SVM>::get_default_grid(ml::SVM_ParamTypes::NU as i32)?;
			let default_coeff = <dyn ml::SVM>::get_default_grid(ml::SVM_ParamTypes::COEF as i32)?;
			let default_grid = <dyn ml::SVM>::get_default_grid(ml::SVM_ParamTypes::DEGREE as i32)?;

			// NOTE (JB) Train SVM
			svm_model.train_auto(
				&cv_train_data,
				10,
				default_c,
				default_gamma,
				default_p,
				default_nu,
				default_coeff,
				default_grid,
				false,
			)?;

			// NOTE (JB) Save SVM
			Model::SVM(svm_model)
		}
		ModelType::RF => {
			let mut forest = <dyn ml::RTrees>::create()?;

			super::pad_auto_trainers::rf_train_auto(&mut forest, &cv_train_data)?;

			Model::RF(forest)
		}
		ModelType::MP => {
			let mut mlp = <dyn ml::ANN_MLP>::create()?;

			super::pad_auto_trainers::mp_train_auto(&mut mlp, &cv_train_data)?;

			Model::MP(mlp)
		}
	};

	Ok(model)
}

/// Creates an OpenCV Mat containing the responses for a NN from a labels Mat.
///
/// # Arguments
///
/// * `train_labels`: Mat containing labels of either 0 or 1
/// * `value`: Value to set responses to. If None, 0.8 it uses.
///
/// returns: Result<Mat, Error>
pub fn create_nn_responses_from_labels(train_labels: &Mat, value: Option<f32>) -> Result<Mat> {
	let mut nn_responses = unsafe { Mat::new_rows_cols(train_labels.rows(), 2, opencv::core::CV_32FC1)? };

	let value = if let Some(value) = value { value } else { 0.8 };

	for i in 0..train_labels.rows() {
		if *train_labels.at::<i32>(i)? == 1 {
			*nn_responses.at_2d_mut::<f32>(i, 0)? = value;
			*nn_responses.at_2d_mut::<f32>(i, 1)? = -value;
		} else {
			*nn_responses.at_2d_mut::<f32>(i, 0)? = -value;
			*nn_responses.at_2d_mut::<f32>(i, 1)? = value;
		}
	}

	Ok(nn_responses)
}

/// Converts a neural network result into an f32 value related to the label
///
/// # Arguments
///
/// * `val1`: textured response
/// * `val2`: non-textured response
///
/// returns: f32
pub fn nn_response_to_label(val1: &f32, val2: &f32) -> f32 {
	if *val1 > 0.8 && *val2 < -0.8 {
		1_f32
	} else if *val1 < -0.8 && *val2 > 0.8 {
		0_f32
	} else {
		let norm_val1 = (val1 + 1.) / 2.;
		let norm_val2 = (val2 + 1.) / 2.;

		let abs_diff = (norm_val1 - norm_val2).abs();

		let mid_diff = if norm_val1.abs() > norm_val2.abs() {
			0.5_f32 + (abs_diff / 2.)
		} else if norm_val1.abs() < norm_val2.abs() {
			0.5_f32 - (abs_diff / 2.)
		} else {
			0.5_f32
		};

		mid_diff
	}
}

/// Generates the filename for a trained PAD model based on the provided arguments.
///
/// # Arguments
///
/// * `model_type`: The type of model
/// * `filter_size`: The size of the BSIF filter
/// * `bit_size`: The bit size of the filter
///
/// returns: String
///
/// # Examples
///
/// ```
/// use opensourceirispad_oxidised::model_details::ModelType;
/// use opensourceirispad_oxidised::pad_detector::generate_model_filename;
///
/// let filename = generate_model_filename(ModelType::SVM, 11, 10);
/// ```
pub fn generate_model_filename(model_type: ModelType, filter_size: i32, bit_size: i32) -> String {
	format!("BSIF-{}-{}-{}.xml", model_type, filter_size, bit_size)
}

/// Converts an extracted histogram feature into an OpenCV Mat (required for live usage)
///
/// # Arguments
///
/// * `feature`: The feature histogram generated by @opensourceirispad_oxidised::bsif_filters::generate_histogram
///
/// returns: Result<Mat, Error>
///
/// # Examples
///
/// ```no_run
/// use opencv::prelude::*;
/// use opensourceirispad_oxidised::bsif_filters::generate_histogram;
/// use opensourceirispad_oxidised::pad_detector::convert_feature_to_cv;
///
/// // This image is default only for the example. Use a
/// let image = Mat::default();
/// let histograms = generate_histogram(&image, 11, 10).unwrap();
///
/// let cv_histogram = convert_feature_to_cv(&histograms).unwrap();
/// ```
pub fn convert_feature_to_cv(feature: &Vec<i32>) -> Result<Mat> {
	let mut output_mat = unsafe { Mat::new_rows_cols(1, feature.len() as i32, opencv::core::CV_32FC1)? };

	feature_to_cv_ref(feature, &mut output_mat)?;

	Ok(output_mat)
}

/// The same as @convert_feature_to_cv, however outputs into a referenced Mat. Can be used with a
/// Region of Interest Mat to store into a larger Mat.
///
/// # Arguments
///
/// * `feature`: The BSIF histogram feature vector. Length must be same as output_mat.cols()
/// * `output_mat`: A reference to a Mat region to output the feature to
///
/// returns: Result<(), Error>
fn feature_to_cv_ref(feature: &Vec<i32>, output_mat: &mut Mat) -> Result<()> {
	if feature.len() != output_mat.cols() as usize || output_mat.rows() != 1 {
		bail!("Feature Length is different than Output Mat");
	}

	// NOTE (JB) Copy features
	for (p, f) in feature.iter().enumerate() {
		*output_mat.at_2d_mut::<f32>(0, p as i32)? = *f as f32;
	}

	// NOTE (JB) Calculate mean/std
	// TODO (JB) Fix this once Scalars are
	let mut mean = Mat::zeros(1, 4, opencv::core::CV_64FC1)?.to_mat()?;
	let mut std_dev = Mat::zeros(1, 4, opencv::core::CV_64FC1)?.to_mat()?;

	opencv::core::mean_std_dev(output_mat, &mut mean, &mut std_dev, &no_array())?;

	// NOTE (JB) Normalize based on mean/std
	for f in 0..feature.len() {
		*output_mat.at_mut::<f32>(f as i32)? =
			(*output_mat.at::<f32>(f as i32)? - *mean.at::<f64>(0)? as f32) / *std_dev.at::<f64>(0)? as f32;
	}

	Ok(())
}

/// Converts a list of histogram features into one OpenCV mat
///
/// # Arguments
///
/// * `features`: A vec of filenames (with '/' replaced with '_') and corresponding feature histogram
/// * `labels`: Optionally create a label mat corresponding to the features. The hashmap is filename (as described above) to label (0 or 1)
/// * `bit_size`: The bit size used to generate the feature vectors
///
/// returns: Result<(Mat, Option<Mat>), Error>
pub fn convert_feature_set_to_cv(
	features: &Vec<(String, Vec<i32>)>,
	labels: Option<&HashMap<String, i32>>,
	bit_size: i32,
) -> Result<(Mat, Option<Mat>)> {
	let features_mat = Mat::zeros(
		features.len() as i32,
		2_i32.pow(bit_size as u32),
		opencv::core::CV_32FC1,
	)?
	.to_mat()?;
	let mut labels_mat = Mat::zeros(features.len() as i32, 1, opencv::core::CV_32SC1)?.to_mat()?;

	let labels_filtered = if let Some(labels) = labels {
		Some(
			labels
				.iter()
				.map(|(filename, label)| (filename.replace("/", "_"), label))
				.collect::<HashMap<_, _>>(),
		)
	} else {
		None
	};

	for (f, (filename, feature)) in features.iter().enumerate() {
		// NOTE (JB) Set Label
		if let Some(labels) = &labels_filtered {
			let label = labels
				.get(&filename.replace("/", "_"))
				.ok_or(anyhow!("Somehow no label for item?"))?;
			*labels_mat.at_mut::<i32>(f as i32)? = **label;
		}

		// NOTE (JB) Copy features
		let mut features_row = features_mat.row(f as i32)?;

		feature_to_cv_ref(feature, &mut features_row)?;
	}

	Ok((features_mat, if labels.is_some() { Some(labels_mat) } else { None }))
}

pub fn convert_feature_set_to_cv_ref_vec(
	features: &Vec<&(String, Vec<i32>)>,
	labels: Option<&HashMap<String, i32>>,
	bit_size: i32,
) -> Result<(Mat, Option<Mat>)> {
	let features_mat = Mat::zeros(
		features.len() as i32,
		2_i32.pow(bit_size as u32),
		opencv::core::CV_32FC1,
	)?
	.to_mat()?;
	let mut labels_mat = Mat::zeros(features.len() as i32, 1, opencv::core::CV_32SC1)?.to_mat()?;

	let labels_filtered = if let Some(labels) = labels {
		Some(
			labels
				.iter()
				.map(|(filename, label)| (filename.replace("/", "_"), label))
				.collect::<HashMap<_, _>>(),
		)
	} else {
		None
	};

	for (f, (filename, feature)) in features.iter().enumerate() {
		// NOTE (JB) Set Label
		if let Some(labels) = &labels_filtered {
			let label = labels
				.get(&filename.replace("/", "_"))
				.ok_or(anyhow!("Somehow no label for training item?: {}", filename))?;
			*labels_mat.at_mut::<i32>(f as i32)? = **label;
		}

		// NOTE (JB) Copy features
		let mut features_row = features_mat.row(f as i32)?;

		feature_to_cv_ref(feature, &mut features_row)?;
	}

	Ok((features_mat, if labels.is_some() { Some(labels_mat) } else { None }))
}

#[cfg(test)]
mod test {
	use super::*;
	use float_cmp::assert_approx_eq;

	#[test]
	fn nn_responses_test() {
		// NOTE (JB) Values above opposing 0.8s should have clear results
		assert_eq!(nn_response_to_label(&1.0, &-1.0), 1.0);
		assert_eq!(nn_response_to_label(&-1.0, &1.0), 0.0);

		assert_eq!(nn_response_to_label(&0.81, &-0.81), 1.0);
		assert_eq!(nn_response_to_label(&-0.81, &0.81), 0.0);

		// NOTE (JB) Same values should have 0.5
		assert_eq!(nn_response_to_label(&0.9, &0.9), 0.5);

		// NOTE (JB) Values otherwise should have mixed results
		assert_approx_eq!(f32, nn_response_to_label(&-0.7, &0.9), 0.1);
		assert_approx_eq!(f32, nn_response_to_label(&0.9, &-0.7), 0.9);

		assert_approx_eq!(f32, nn_response_to_label(&0.9, &0.85), 0.5125);
		assert_approx_eq!(f32, nn_response_to_label(&0.85, &0.9), 0.4875);
	}

	#[test]
	fn majority_vote_inconsistent_error() {
		let inconsistent_vec = vec![vec![1, 0], vec![1]];

		let result = majority_vote(&inconsistent_vec);

		assert!(result.is_err());
	}

	#[test]
	fn majority_vote_consistent_vec_result_test() {
		let consistent_vec = vec![vec![1, 1], vec![1, 0], vec![1, 1]];

		let result = majority_vote(&consistent_vec);

		assert!(result.is_ok(), "Error during majority vote: {}", result.unwrap_err());

		let vote_results = result.unwrap();

		assert_eq!(vote_results, vec![1, 1], "Incorrect Results");
	}

	#[test]
	fn majority_vote_vec_float_result_test() {

		let consistent_vec = vec![vec![1, 1], vec![1, 0], vec![1, 1]];

		let result = majority_vote_float(&consistent_vec);

		assert!(result.is_ok(), "Error during majority vote: {}", result.unwrap_err());

		let vote_results = result.unwrap();

		assert_eq!(vote_results, vec![1.0, (2.0/3.0)], "Incorrect Results");
	}

	#[test]
	fn pad_detector_load_test() -> Result<()> {
		let config_file = "best_model_details.toml";
		let config_file_path = std::path::Path::new(config_file);

		// Can only run test if model config exists
		assert!(
			config_file_path.exists(),
			"best_model_details.toml must exist for test to work"
		);

		let loaded_model = IrisPADModel::try_new_from_toml_file(config_file)?;

		// match loaded_model {
		// 	Ok(_) => {assert!(true);}
		// 	Err(e) => {
		// 		assert!(false, "Unable to load model: {}", e);
		// 	}
		// }

		Ok(())
	}

	#[test]
	fn pad_detector_test_set_first_10() -> Result<()> {
		let config = &crate::config::CONFIG;

		let config_file = "best_model_details.toml";
		let config_file_path = std::path::Path::new(config_file);

		// Can only run test if model config exists
		assert!(
			config_file_path.exists(),
			"best_model_details.toml must exist for test to work"
		);

		let primary_config_file = "config.toml";
		let primary_config_path = std::path::Path::new(primary_config_file);

		// Requires custom primary config
		assert!(primary_config_path.exists(), "config.toml must exist for test to work");

		let loaded_model = IrisPADModel::try_new_from_toml_file(config_file)?;

		let testing_set_list = crate::csv_helpers::load_csv_file_list(config.evaluation.test_set_list.as_ref())?
			.into_iter()
			.take(10)
			.map(|(filen, _)| {
				let img = opencv::imgcodecs::imread(&filen, 0);

				match img {
					Ok(i) => Ok(i),
					Err(e) => Err(anyhow!("Unable to load {}", &filen)),
				}
			})
			.collect::<Result<Vec<Mat>>>();

		let testing_set_mats = match testing_set_list {
			Ok(t) => t,
			Err(e) => {
				assert!(false, "Error while loading images: {}", e);
				unreachable!();
			}
		};

		let model_res = loaded_model.predict(&testing_set_mats)?;

		Ok(())
	}
}
