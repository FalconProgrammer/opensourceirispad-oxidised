use anyhow::Result;
use itertools::{izip, Itertools};
use opencv::core::no_array;
use opencv::prelude::*;
use rand::prelude::*;
use rayon::prelude::*;

struct MatSyncWrapper(Mat);

unsafe impl Sync for MatSyncWrapper {}
unsafe impl Send for MatSyncWrapper {}

/// Trains an OpenCV RTrees model based on 10 K-Folds testing
///
/// # Arguments
///
/// * `model`:
/// * `data`:
///
/// returns: Result<(), Error>
pub fn rf_train_auto(
	model: &mut opencv::core::Ptr<dyn opencv::ml::RTrees>,
	data: &opencv::core::Ptr<dyn opencv::ml::TrainData>,
) -> Result<()> {
	use opencv::ml;
	use rand::prelude::*;

	let k_folds = 10;

	// 10 folds times 20 trials = 200 total training cycles
	let training_depth = [1, 5, 10, 15, 20, 25];
	let percent_of_traindata = [1., 1.5, 2., 2.5];

	// get training data
	let samples = data.get_train_samples(ml::ROW_SAMPLE, true, true)?;
	let responses = data.get_train_responses()?;

	let samples_cols = samples.cols();
	let responses_cols = responses.cols();

	let sample_count = samples.rows();

	// Randomly permute training samples
	let mut rng = rand::rngs::StdRng::seed_from_u64(12345);

	let (k_idx, n0, n1) = prepare_folds(&samples, &responses, k_folds, &mut rng)?;

	// calculate number of samples per fold
	let number_per_fold = (n0 + k_folds / 2) / k_folds + (n1 + k_folds / 2) / k_folds;

	// Mat's contain pointers and are "unsafe" between threads. Using MatSyncWrapper 'hack'
	let samples_lock = std::sync::Arc::new(crossbeam::sync::ShardedLock::new(MatSyncWrapper(samples)));
	let responses_lock = std::sync::Arc::new(crossbeam::sync::ShardedLock::new(MatSyncWrapper(responses)));

	let training_combinations = training_depth
		.iter()
		.cartesian_product(percent_of_traindata.iter())
		.map(|(t_d, p_t)| (*t_d, *p_t, samples_lock.clone(), responses_lock.clone()))
		.collect_vec();

	let results = training_combinations
		.par_iter()
		.map(|(t_d, p_t, samples_lock, responses_lock)| {
			let mut ccr = Vec::<f32>::new();

			for k in 0..k_folds {
				// load folds minus k

				let train_samples = unsafe {
					Mat::new_rows_cols(
						((k_folds - 1) * number_per_fold) as i32,
						samples_cols,
						opencv::core::CV_32FC1,
					)?
				};
				let mut train_classes = unsafe {
					Mat::new_rows_cols(
						((k_folds - 1) * number_per_fold) as i32,
						responses_cols,
						opencv::core::CV_32SC1,
					)?
				};

				let mut samples_added = 0;

				for l in 0..k_folds {
					if l != k {
						let current_fold = &k_idx[l as usize];

						let samples = &samples_lock.read().unwrap().0;
						let responses = &responses_lock.read().unwrap().0;

						for sample_num in current_fold {
							anyhow::ensure!(
								*sample_num < samples.rows(),
								anyhow::anyhow!("RF AutoTrainer: Sample Num too large for Samples Row")
							);
							anyhow::ensure!(
								*sample_num < responses.rows(),
								anyhow::anyhow!("RF AutoTrainer: Sample Num too large for Responses Row")
							);
							anyhow::ensure!(
								samples_added < train_classes.rows(),
								anyhow::anyhow!("RF AutoTrainer: Sample Num too large for TrainClasses Row")
							);

							anyhow::ensure!(
								*sample_num >= 0,
								anyhow::anyhow!("RF AutoTrainer: Sample Num too small for Samples Row")
							);

							samples
								.row(*sample_num)?
								.copy_to(&mut train_samples.row(samples_added)?)?;
							*train_classes.at_mut::<i32>(samples_added)? = *responses.at::<i32>(*sample_num)?;
							samples_added += 1;
						}
					}
				}

				// Place into trainData
				let training_data = <dyn ml::TrainData>::create(
					&train_samples,
					ml::ROW_SAMPLE,
					&train_classes,
					&no_array(),
					&no_array(),
					&no_array(),
					&no_array(),
				)?;

				// Create temporary forest
				let mut tmp_forest = <dyn ml::RTrees>::create()?;
				tmp_forest.set_max_depth(*t_d)?;
				tmp_forest.set_min_sample_count(sample_count * *p_t as i32 / 100)?;

				// Train the model
				tmp_forest.train_with_data(&training_data, 0)?;

				// Load final fold to test
				let final_fold = &k_idx[k as usize];

				let test_samples =
					unsafe { Mat::new_rows_cols(final_fold.len() as i32, samples_cols, opencv::core::CV_32FC1)? };
				let mut test_classes =
					unsafe { Mat::new_rows_cols(final_fold.len() as i32, responses_cols, opencv::core::CV_32SC1)? };

				let samples = &samples_lock.read().unwrap().0;
				let responses = &responses_lock.read().unwrap().0;

				for (n, sample_num) in final_fold.iter().enumerate() {
					anyhow::ensure!(
						*sample_num < samples.rows(),
						anyhow::anyhow!("RF AutoTrainer: Sample Num too large for Samples Row")
					);
					anyhow::ensure!(
						*sample_num < responses.rows(),
						anyhow::anyhow!("RF AutoTrainer: Sample Num too large for Responses Row")
					);
					anyhow::ensure!(
						(n as i32) < test_classes.rows(),
						anyhow::anyhow!("RF AutoTrainer: Sample Num too large for TestClasses Row")
					);

					anyhow::ensure!(
						*sample_num >= 0,
						anyhow::anyhow!("RF AutoTrainer: Sample Num too small for Samples Row")
					);
					anyhow::ensure!(
						(n as i32) >= 0,
						anyhow::anyhow!("RF AutoTrainer: Sample Num too small for TestClasses Row")
					);

					samples
						.row(*sample_num)?
						.copy_to(
							&mut test_samples
								.row(n as i32)
								.expect("RF AutoTrainer: Invalid Test Samples Number"),
						)
						.expect("RF AutoTrainer: Unable to copy to test_samples due to invalid row number");

					*test_classes.at_mut::<i32>(n as i32)? = *responses.at::<i32>(*sample_num)?;
					samples_added += 1;
				}

				drop(samples);
				drop(responses);

				// Test model
				let mut predictions = Mat::default();
				tmp_forest.predict(&test_samples, &mut predictions, 0)?;

				// Determine number incorrect
				let num_incorrect = izip!(predictions.iter::<f32>()?, test_classes.iter::<i32>()?)
					.filter(|(p, a)| p.1 as i32 != a.1)
					.count() as i32;

				// Output Accuracy
				let ccr_single = 100_f32 - (num_incorrect as f32 / test_classes.rows() as f32) * 100_f32;

				ccr.push(ccr_single);
			}

			let total = ccr.iter().sum::<f32>();
			let mean_performance = total / ccr.len() as f32;

			Ok((mean_performance, *t_d, *p_t as i32))
		})
		.collect::<Result<Vec<(f32, i32, i32)>>>()?;

	let (_best_ccr, best_depth, best_count) = results
		.iter()
		.max_by(|(ccr_a, _, _), (ccr_b, _, _)| ccr_a.partial_cmp(ccr_b).unwrap())
		.unwrap();

	// train with best parameters
	model.set_max_depth(*best_depth)?;
	model.set_min_sample_count(sample_count * *best_count / 100)?;

	model.train_with_data(data, 0)?;

	Ok(())
}

/// Trains an OpenCV ANN_MLP model based on 10 K-Folds testing
///
/// # Arguments
///
/// * `model`:
/// * `data`:
///
/// returns: Result<(), Error>
pub fn mp_train_auto(
	model: &mut opencv::core::Ptr<dyn opencv::ml::ANN_MLP>,
	data: &opencv::core::Ptr<dyn opencv::ml::TrainData>,
) -> Result<()> {
	use opencv::ml;
	use rand::prelude::*;

	let k_folds = 10;

	// 10 folds times 4 trials = 40 total training cycles
	let hidden_layer_multiplier = [1, 2, 4];

	// get training data
	let nn_data = {
		let samples = data.get_train_samples(ml::ROW_SAMPLE, true, true)?;
		let responses = data.get_train_responses()?;

		let nn_responses = super::pad_detector::create_nn_responses_from_labels(&responses, Some(1.0))?;

		<dyn opencv::ml::TrainData>::create(
			&samples,
			opencv::ml::ROW_SAMPLE,
			&nn_responses,
			&no_array(),
			&no_array(),
			&no_array(),
			&no_array(),
		)?
	};

	let samples = nn_data.get_train_samples(ml::ROW_SAMPLE, true, true)?;
	let responses = nn_data.get_train_responses()?;

	let samples_cols = samples.cols();
	let responses_cols = responses.cols();

	let _sample_count = samples.rows();

	// Randomly permute training samples
	let mut rng = rand::rngs::StdRng::seed_from_u64(12345);

	let (k_idx, n0, n1) = prepare_folds_mp(&samples, &responses, k_folds, &mut rng)?;

	// calculate number of samples per fold
	let number_per_fold = (n0 + k_folds / 2) / k_folds + (n1 + k_folds / 2) / k_folds;

	// Mat's contain pointers and are "unsafe" between threads. Using MatSyncWrapper 'hack'
	let samples_lock = std::sync::Arc::new(crossbeam::sync::ShardedLock::new(MatSyncWrapper(samples)));
	let responses_lock = std::sync::Arc::new(crossbeam::sync::ShardedLock::new(MatSyncWrapper(responses)));

	let combinations = hidden_layer_multiplier
		.iter()
		.map(|h_d| (*h_d, samples_lock.clone(), responses_lock.clone()))
		.collect_vec();

	let results = combinations
		.par_iter()
		.map(|(h_d, samples_lock, responses_lock)| {
			let mut ccr = Vec::<f32>::new();

			println!("Started Combination {}", h_d);

			for k in 0..k_folds {
				// load folds minus k

				let train_samples = unsafe {
					Mat::new_rows_cols(
						((k_folds - 1) * number_per_fold) as i32,
						samples_cols,
						opencv::core::CV_32FC1,
					)?
				};
				let mut train_classes = unsafe {
					Mat::new_rows_cols(
						((k_folds - 1) * number_per_fold) as i32,
						responses_cols,
						opencv::core::CV_32FC1,
					)?
				};

				let mut samples_added = 0;

				for l in 0..k_folds {
					if l != k {
						let current_fold = &k_idx[l as usize];

						let samples = &samples_lock.read().unwrap().0;
						let responses = &responses_lock.read().unwrap().0;

						for sample_num in current_fold {
							samples
								.row(*sample_num)?
								.copy_to(&mut train_samples.row(samples_added)?)?;

							*train_classes.at_2d_mut::<f32>(samples_added, 0)? =
								*responses.at_2d::<f32>(*sample_num, 0)?;
							*train_classes.at_2d_mut::<f32>(samples_added, 1)? =
								*responses.at_2d::<f32>(*sample_num, 1)?;

							samples_added += 1;
						}
					}
				}

				// Place into trainData
				let training_data = <dyn ml::TrainData>::create(
					&train_samples,
					ml::ROW_SAMPLE,
					&train_classes,
					&no_array(),
					&no_array(),
					&no_array(),
					&no_array(),
				)?;

				let mut layer_sizes = unsafe { Mat::new_rows_cols(3, 1, opencv::core::CV_32SC1)? };
				*layer_sizes.at_mut::<i32>(0)? = train_samples.cols();
				*layer_sizes.at_mut::<i32>(1)? = train_samples.cols() * h_d;
				*layer_sizes.at_mut::<i32>(2)? = train_classes.cols();

				let mut tmp_mp = <dyn opencv::ml::ANN_MLP>::create()?;
				tmp_mp.set_layer_sizes(&layer_sizes)?;
				tmp_mp.set_train_method(opencv::ml::ANN_MLP_TrainingMethods::BACKPROP as i32, 0., 0.)?;
				tmp_mp.set_activation_function(opencv::ml::ANN_MLP_ActivationFunctions::SIGMOID_SYM as i32, 0., 0.)?;

				tmp_mp.train_with_data(&training_data, 0)?;

				// Load final fold to test
				let final_fold = &k_idx[k as usize];

				let test_samples =
					unsafe { Mat::new_rows_cols(final_fold.len() as i32, samples_cols, opencv::core::CV_32FC1)? };
				let mut test_classes =
					unsafe { Mat::new_rows_cols(final_fold.len() as i32, responses_cols, opencv::core::CV_32FC1)? };

				let samples = &samples_lock.read().unwrap().0;
				let responses = &responses_lock.read().unwrap().0;

				for (n, sample_num) in final_fold.iter().enumerate() {
					samples.row(*sample_num)?.copy_to(&mut test_samples.row(n as i32)?)?;

					*test_classes.at_2d_mut::<f32>(n as i32, 0)? = *responses.at_2d::<f32>(n as i32, 0)?;
					*test_classes.at_2d_mut::<f32>(n as i32, 1)? = *responses.at_2d::<f32>(n as i32, 1)?;

					samples_added += 1;
				}

				drop(samples);
				drop(responses);

				// Test model
				let mut predictions = Mat::default();
				tmp_mp.predict(&test_samples, &mut predictions, 0)?;

				// Determine number incorrect
				let mut num_incorrect = 0;

				for p in 0..test_classes.rows() {
					let res_0 = predictions.at_2d::<f32>(p, 0)?;
					let res_1 = predictions.at_2d::<f32>(p, 1)?;

					let test_res = test_classes.at_2d::<f32>(p, 0)?;

					if *res_0 > 0.8 && *res_1 < -0.8 {
						if *test_res == -1.0 {
							num_incorrect += 1;
						}
					} else if *res_0 < -0.8 && *res_1 > 0.8 {
						if *test_res == 1.0 {
							num_incorrect += 1;
						}
					} else {
						num_incorrect += 1
					}
				}

				// Output Accuracy
				let ccr_single = 100_f32 - (num_incorrect as f32 / test_classes.rows() as f32) * 100_f32;

				ccr.push(ccr_single);
			}

			let total = ccr.iter().sum::<f32>();
			let mean_performance = total / ccr.len() as f32;

			println!("Finished Combination {}", h_d);

			Ok((mean_performance, *h_d))
		})
		.collect::<Result<Vec<(f32, i32)>>>()?;

	// testing parameters
	let (_best_ccr, best_hl_multiplier) = results
		.iter()
		.max_by(|(ccr_a, _), (ccr_b, _)| ccr_a.partial_cmp(ccr_b).unwrap())
		.unwrap();

	let mut layer_sizes = unsafe { Mat::new_rows_cols(3, 1, opencv::core::CV_32SC1)? };
	*layer_sizes.at_mut::<i32>(0)? = samples_cols;
	*layer_sizes.at_mut::<i32>(1)? = samples_cols * best_hl_multiplier;
	*layer_sizes.at_mut::<i32>(2)? = responses_cols;

	model.set_layer_sizes(&layer_sizes)?;
	model.set_train_method(opencv::ml::ANN_MLP_TrainingMethods::BACKPROP as i32, 0., 0.)?;
	model.set_activation_function(opencv::ml::ANN_MLP_ActivationFunctions::SIGMOID_SYM as i32, 0., 0.)?;

	model.train_with_data(&nn_data, 0)?;

	Ok(())
}

/// Prepares K folds for use as part of training. Returns:
///
/// - Vec of folds (with Vec of indices to samples)
/// - Number of Bonafide Items within Samples
/// - Number of Attack Items within Samples
///
/// # Arguments
///
/// * `samples`: Mat containing all samples to fold over
/// * `responses`: Mat containing the labels to fold
/// * `k_folds`: The number of k folds to generate
/// * `rng`: Random number generator to determine randomness
///
/// returns: Result<(Vec<Vec<i32, Global>, Global>, usize, usize), Error>
fn prepare_folds(
	samples: &Mat,
	responses: &Mat,
	k_folds: usize,
	rng: &mut rand::rngs::StdRng,
) -> Result<(Vec<Vec<i32>>, usize, usize)> {
	let sample_count = samples.rows();

	// reshuffle the training set in such a way that
	// instances of each class are divided more or less evenly
	// between the k_fold parts.
	let (s_idx_0, s_idx_1) = {
		let mut s_idx = (0..sample_count).collect::<Vec<_>>();
		s_idx.shuffle(rng);

		let mut s_idx_0 = Vec::<i32>::new();
		let mut s_idx_1 = Vec::<i32>::new();

		for i in s_idx.iter() {
			if *responses.at::<i32>(*i)? == 0 {
				s_idx_0.push(*i);
			} else {
				s_idx_1.push(*i);
			}
		}

		(s_idx_0, s_idx_1)
	};

	let n0 = s_idx_0.len();
	let n1 = s_idx_1.len();
	let mut a0 = 0;
	let mut a1 = 0;

	let k_idx = (0..k_folds)
		.map(|k| {
			let mut s_idx = Vec::<i32>::new();

			let b0 = ((k + 1) * n0 + k_folds / 2) / k_folds;
			let b1 = ((k + 1) * n1 + k_folds / 2) / k_folds;

			let a = s_idx.len();
			let _b = a + (b0 - a0) + (b1 - a1);

			for i in a0..b0 {
				s_idx.push(s_idx_0[i]);
			}

			for i in a1..b1 {
				s_idx.push(s_idx_1[i]);
			}

			s_idx.shuffle(rng);

			a0 = b0;
			a1 = b1;

			s_idx
		})
		.collect_vec();

	Ok((k_idx, n0, n1))
}
/// Prepares K folds for use as part of training. Returns:
///
/// - Vec of folds (with Vec of indices to samples)
/// - Number of Bonafide Items within Samples
/// - Number of Attack Items within Samples
///
/// # Arguments
///
/// * `samples`: Mat containing all samples to fold over
/// * `responses`: Mat containing the labels to fold
/// * `k_folds`: The number of k folds to generate
/// * `rng`: Random number generator to determine randomness
///
/// returns: Result<(Vec<Vec<i32, Global>, Global>, usize, usize), Error>
fn prepare_folds_mp(
	samples: &Mat,
	responses: &Mat,
	k_folds: usize,
	rng: &mut rand::rngs::StdRng,
) -> Result<(Vec<Vec<i32>>, usize, usize)> {
	let sample_count = samples.rows();

	// reshuffle the training set in such a way that
	// instances of each class are divided more or less evenly
	// between the k_fold parts.
	let (s_idx_0, s_idx_1) = {
		let mut s_idx = (0..sample_count).collect::<Vec<_>>();
		s_idx.shuffle(rng);

		let mut s_idx_0 = Vec::<i32>::new();
		let mut s_idx_1 = Vec::<i32>::new();

		for i in s_idx.iter() {
			if *responses.at_2d::<f32>(*i, 0)? == -0.8 && *responses.at_2d::<f32>(*i, 1)? == 0.8 {
				s_idx_0.push(*i);
			} else {
				s_idx_1.push(*i);
			}
		}

		(s_idx_0, s_idx_1)
	};

	let n0 = s_idx_0.len();
	let n1 = s_idx_1.len();
	let mut a0 = 0;
	let mut a1 = 0;

	let k_idx = (0..k_folds)
		.map(|k| {
			let mut s_idx = Vec::<i32>::new();

			let b0 = ((k + 1) * n0 + k_folds / 2) / k_folds;
			let b1 = ((k + 1) * n1 + k_folds / 2) / k_folds;

			let a = s_idx.len();
			let _b = a + (b0 - a0) + (b1 - a1);

			for i in a0..b0 {
				s_idx.push(s_idx_0[i]);
			}

			for i in a1..b1 {
				s_idx.push(s_idx_1[i]);
			}

			s_idx.shuffle(rng);

			a0 = b0;
			a1 = b1;

			s_idx
		})
		.collect_vec();

	Ok((k_idx, n0, n1))
}
