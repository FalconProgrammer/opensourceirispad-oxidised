use anyhow::{bail, Result};

use crate::bsif_filters::generate_histogram;
use cv::prelude::*;
use log::warn;
use opencv as cv;

use opencv::imgcodecs::{imread, IMREAD_GRAYSCALE};
use rayon::prelude::*;

/// Extracts and saves BSIF histogram features from a file list to an HDF5 file
///
/// # Arguments
///
/// * `file_list`:
/// * `output_hdf5_file`:
/// * `filter_size`:
/// * `bit_size`:
/// * `down_sample`:
///
/// returns: Result<(), Error>
pub fn extract_feature_histograms_to_hdf5_file(
	file_list: &Vec<String>,
	output_hdf5_file: &str,
	filter_size: u8,
	bit_size: u8,
	down_sample: bool,
) -> Result<()> {
	let file_id = hdf5::File::create(output_hdf5_file)?;

	let histogram_iter = extract_histogram_iter(file_list, filter_size, bit_size, down_sample);

	for histogram in histogram_iter {
		match &histogram {
			Ok((f, histogram)) => {
				let path = f.clone().replace("/", "_");
				let _dset = file_id
					.new_dataset_builder()
					.with_data(&histogram)
					.create(path.as_ref())?;
			}
			Err(e) => {
				warn!("Extracting Feature List Error: {}", e);
			}
		}
	}

	Ok(())
}

/// Saves extracted BSIF histogram features to an HDF5 file
///
/// # Arguments
///
/// * `histograms`:
/// * `output_hdf5_file`:
///
/// returns: Result<(), Error>
pub fn save_feature_histograms_to_hdf5(histograms: &Vec<(String, Vec<i32>)>, output_hdf5_file: &str) -> Result<()> {
	let file_id = hdf5::File::create(output_hdf5_file)?;

	for (f, histogram) in histograms.iter() {
		let path = f.clone().replace("/", "_");
		let _dset = file_id
			.new_dataset_builder()
			.with_data(&histogram)
			.create(path.as_ref())?;
	}

	Ok(())
}

/// Loads up BSIF histogram features from an HDF5 file. Each histogram must be named after their full
/// file path with '/' replaced with '_'
///
/// # Arguments
///
/// * `input_hdf5_file`: The file to load the features from
///
/// returns: Result<Vec<(String, Vec<i32>)>, Error>
pub fn load_feature_histograms_from_hdf5(input_hdf5_file: &str) -> Result<Vec<(String, Vec<i32>)>> {
	let file_id = hdf5::File::open(input_hdf5_file)?;

	let histograms = file_id
		.member_names()?
		.iter()
		.map(|name| -> Result<(String, Vec<i32>)> {
			let dataset = file_id.dataset(name)?;

			let histogram = dataset
				.read_slice_1d(hdf5::Selection::All)?
				.iter()
				.cloned()
				.collect::<Vec<i32>>();

			Ok((name.clone(), histogram))
		})
		.filter_map(|hist_res| match hist_res {
			Ok(h) => Some(h),
			Err(e) => {
				warn!("Error when loading from HDF5 file: {}", e);
				None
			}
		})
		.collect::<Vec<_>>();

	Ok(histograms)
}

/// Extracts a Vec of BSIF histogram features from a list of filenames for a set BSIF filter/bit size
///
/// # Arguments
///
/// * `file_list`: List of files
/// * `filter_size`: BSIF filter size
/// * `bit_size`: BSIF bit size
/// * `down_sample`: Will downsample the input images by half
///
/// returns: Vec<(String, Vec<i32>)>
pub fn extract_file_feature_histograms_to_vec(
	file_list: &Vec<String>,
	filter_size: u8,
	bit_size: u8,
	down_sample: bool,
) -> Vec<(String, Vec<i32>)> {
	file_list
		.par_iter()
		.map(move |i: &String| -> Result<(&String, Vec<i32>)> {
			let image = match imread(i, IMREAD_GRAYSCALE) {
				Ok(i) => i,
				Err(e) => {
					bail!("Extracting Feature List Error for {} when loading image: {}", i, &e);
				}
			};

			let histogram = extract_mat_feature_histograms_downsample(&image, filter_size, bit_size, down_sample);

			let histogram = if let Ok(hist) = histogram {
				hist
			} else {
				bail!(
					"Unable to extract for file [ {} ] with filter {} and bit {}",
					i,
					filter_size,
					bit_size
				);
			};

			Ok((i, histogram))
		})
		.filter_map(|h| match h {
			Ok((name, hist)) => Some((name.clone(), hist)),
			Err(_) => None,
		})
		.collect::<Vec<_>>()
}

pub fn extract_mat_feature_histograms_downsample(
	image: &Mat,
	filter_size: u8,
	bit_size: u8,
	down_sample: bool,
) -> Result<Vec<i32>> {
	if down_sample {
		let mut down_image = Mat::default();

		cv::imgproc::pyr_down(
			&image,
			&mut down_image,
			cv::core::Size::new(image.cols() / 2, image.rows() / 2),
			cv::core::BORDER_DEFAULT,
		)?;

		generate_histogram(&down_image, filter_size, bit_size)
	} else {
		generate_histogram(&image, filter_size, bit_size)
	}
}

pub fn extract_mat_feature_histograms(image: &Mat, filter_size: u8, bit_size: u8) -> Result<Vec<i32>> {
	let (down_sample, act_filter_size) = if filter_size % 2 == 0 {
		(true, filter_size / 2)
	} else {
		(false, filter_size)
	};

	extract_mat_feature_histograms_downsample(image, act_filter_size, bit_size, down_sample)
}

/// Creates an iterator over generated histogram features
///
/// # Arguments
///
/// * `file_list`: List of files
/// * `filter_size`: BSIF filter size
/// * `bit_size`: BSIF bit size
/// * `down_sample`: Will downsample the input images by half
///
/// returns: impl Iterator<Item=Result<(&String, Vec<i32>), Error>>
fn extract_histogram_iter<C>(
	file_list: &C,
	filter_size: u8,
	bit_size: u8,
	down_sample: bool,
) -> impl Iterator<Item = Result<(&String, Vec<i32>)>>
where
	for<'a> &'a C: IntoIterator<Item = &'a String>,
{
	file_list
		.into_iter()
		.map(move |i: &String| -> Result<(&String, Vec<i32>)> {
			let image = match imread(i, IMREAD_GRAYSCALE) {
				Ok(i) => i,
				Err(e) => {
					warn!("Extracting Feature List Error for {} when loading image: {}", i, &e);
					return Result::Err(anyhow::Error::new(e));
				}
			};

			let histogram = extract_mat_feature_histograms_downsample(&image, filter_size, bit_size, down_sample)?;

			Ok((i, histogram))
		})
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn test_histogram() {
		let features = load_feature_histograms_from_hdf5(&format!(
			"{}/test_res/bla_filter_11_11_10.hdf5",
			env!("CARGO_MANIFEST_DIR")
		))
		.expect("Unable to find example features");
		let (_, cpp_feat) = features.get(0).unwrap();

		let image = imread(
			&format!("{}/test_res/000001.tiff", env!("CARGO_MANIFEST_DIR")),
			IMREAD_GRAYSCALE,
		)
		.expect("Unable to load test image");

		assert_eq!(false, image.empty());

		let rust_hist = crate::bsif_filters::generate_histogram(&image, 11, 10).expect("Unable to extract histogram");

		assert_eq!(*cpp_feat, rust_hist);
	}
}
