use anyhow::bail;
use itertools::izip;
use serde::{Deserialize, Serialize};
use strum::EnumString;

/// An enum for the type of model (used in configuration/parameters)
#[derive(EnumString, strum::Display, Copy, Clone, Eq, PartialEq)]
pub enum ModelType {
	#[strum(serialize = "svm")]
	SVM,
	#[strum(serialize = "rf")]
	RF,
	#[strum(serialize = "mp")]
	MP,
}

/// Contains the parameters used to train/run a model
#[derive(Copy, Clone)]
pub struct ModelDetails {
	pub filter_size: i32,
	pub bit_size: i32,
	pub model_type: ModelType,
}

/// The details of all models used to train/run the overall PAD
pub struct AllModelDetails {
	pub model_details: Vec<ModelDetails>,
	pub majority_voting: bool,
	pub model_directory: String,
}

/// A serializable version of @AllModelDetails
#[derive(Serialize, Deserialize)]
pub struct SerializableModelDetails {
	pub filter_sizes: Vec<i32>,
	pub bit_sizes: Vec<i32>,
	pub model_type: Vec<String>,
	pub model_directory: String,
	pub majority_voting: bool,
}

impl TryFrom<&SerializableModelDetails> for AllModelDetails {
	type Error = anyhow::Error;

	/// Convert from SerializableModelDetails (which can be parsed from file) into usable AllModelDetails
	fn try_from(value: &SerializableModelDetails) -> Result<Self, Self::Error> {
		if value.filter_sizes.len() != value.model_type.len() || value.filter_sizes.len() != value.bit_sizes.len() {
			bail!(
				"Model {} / Filter {} / Bit {} Lengths are not equal.",
				value.model_type.len(),
				value.filter_sizes.len(),
				value.bit_sizes.len()
			);
		}

		let model_details = izip!(&value.filter_sizes, &value.bit_sizes, &value.model_type)
			.map(|(filter_size, bit_size, model)| ModelDetails {
				filter_size: *filter_size,
				bit_size: *bit_size,
				model_type: model.parse().unwrap(),
			})
			.collect::<Vec<_>>();

		Ok(AllModelDetails {
			model_details,
			majority_voting: value.majority_voting,
			model_directory: value.model_directory.clone(),
		})
	}
}

impl From<&crate::config::CONFIG> for SerializableModelDetails {
	/// Parse the config for the main program into the Serlialiable struct.
	fn from(value: &crate::config::CONFIG) -> Self {
		SerializableModelDetails {
			filter_sizes: value.model_info.filter_sizes.iter().map(|v| *v as i32).collect(),
			bit_sizes: value.model_info.bit_sizes.iter().map(|v| *v as i32).collect(),
			model_type: value.model_info.model_type.iter().map(|v| v.to_string()).collect(),
			majority_voting: value.model_info.majority_voting,
			model_directory: value.model_info.model_directory.to_string(),
		}
	}
}

impl From<&AllModelDetails> for SerializableModelDetails {
	/// Convert into a SerializableModelDetails so it can be serialized to file/string.
	fn from(value: &AllModelDetails) -> SerializableModelDetails {
		let filter_sizes = value.model_details.iter().map(|a| a.filter_size).collect::<Vec<_>>();
		let bit_sizes = value.model_details.iter().map(|a| a.bit_size).collect::<Vec<_>>();
		let model_type = value
			.model_details
			.iter()
			.map(|a| a.model_type.to_string())
			.collect::<Vec<_>>();

		SerializableModelDetails {
			filter_sizes,
			bit_sizes,
			model_type,
			majority_voting: value.majority_voting,
			model_directory: value.model_directory.clone(),
		}
	}
}
