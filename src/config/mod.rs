mod config_gen;

use std::path::Path;

fn get_config() -> config_gen::Config {
	let p = Path::new("config.toml");

	if p.exists() {
		let file_contents = std::fs::read_to_string(p).unwrap();
		let config = toml::from_str(&file_contents).unwrap();

		return config;
	} else {
		return config_gen::CONFIG;
	}
}

lazy_static! {
	pub static ref CONFIG: config_gen::Config = get_config();
}
