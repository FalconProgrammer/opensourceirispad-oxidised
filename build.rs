use itertools::Itertools;
use std::path::Path;

fn main() {
	let destination = "src/config/config_gen.rs";

	config_struct::create_struct(
		"default_config/config.toml",
		&destination,
		&config_struct::StructOptions::serde_default(),
	)
	.unwrap();

	// NOTE (JB) Modify contents to allow using default values when not all parameters specified
	let mut contents: Vec<String> = std::fs::read_to_string(Path::new(&destination))
		.unwrap()
		.split("\n")
		.map(|s| String::from(s))
		.collect();

	let pos: Vec<usize> = contents.iter().positions(|c| c.contains("pub struct Config")).collect();

	let mut add = 0 as usize;

	for p in pos {
		contents.insert(p + add, String::from(r##"#[serde(default, deny_unknown_fields)]"##));
		add += 1;
	}

	let mut new_contents = contents.join("\n");

	// NOTE (JB) Specify default
	new_contents.push_str(
		r##"

impl Default for Config {
    fn default() -> Self {
        CONFIG
    }
}
"##,
	);

	// NOTE (JB) Write new version
	std::fs::write(Path::new(&destination), new_contents).unwrap();
}
