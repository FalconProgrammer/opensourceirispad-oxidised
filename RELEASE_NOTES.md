# Release Notes

## 0.3.2

- Added DOI badge

## 0.3.1

- Fixed versioning

## 0.3.0

- Added README.md
- Added some extra error checking for bad configuration
- Added export of results file containing error rates of all combinations of models when ordered

## 0.2.5

- Fixed publicity of IrisPADModel

## 0.2.4

- Added a float predict/majority vote to get confidence value

## 0.2.3

- Fixed warnings
- Added license

## 0.2.2

- Forgot to bump version number in 0.2.1

## 0.2.1

- Refactored filter extraction code.
- Fixed Iris PAD model code
- Added tests for Iris PAD and Majority Vote

## 0.2.0

- Added parallel loading of extracted bsif filters
- Parallelized RF and MP training
- Bug fixes to make the main program work
- Updated default config with all possible bsif filters
- Added option to main program to print all bsif filter combinations

## 0.1.0

- Initial untested port
